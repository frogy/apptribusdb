from pyspark import SQLContext
sql = SQLContext(sc)
users=sql.read.json("/mnt/nfs2/jian/userinfos/")
users.cache()
userfrequency=users.select("user_UUID").map(lambda l:l.user_UUID).filter(lambda l:l!=None).filter(lambda l:l!="").groupBy(lambda l:l).map(lambda l:[l[0],len(l[1])])
