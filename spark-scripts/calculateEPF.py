# !/usr/bin/env python
# -*- coding:utf-8 -*-
from datetime import date,datetime,timedelta
from pyspark import SparkConf,SparkContext
from pyspark.sql import SQLContext,Row
import os
import re
from pyspark import SQLContext
import shutil
import argparse
from tendo import singleton
import logging
import codecs


def writeResult(inFilePath, inList):
    #fp=codecs.open(inFilePath,mode="wb",encoding="utf-8")
    fp=open(inFilePath,"wb")
    fp.write(codecs.BOM_UTF8)
    for k in inList:
        if type(k)==str or type(k)==unicode or type(k)==int or type(k)==long:
            fp.write(unicode(k).encode('utf-8'))
        else :
            for j in k:
                #print j
                #print (str(j)+u',').encode('ascii')
                fp.write((unicode(j)+u',').encode('utf-8'))
        fp.write (u'\n')
    fp.close()


def convertticks(inTimeStr):
    return datetime(1970,1,1)+timedelta(milliseconds=int(inTimeStr))

joindict={
    "app":['export_date','application_id','title','recommended_age','artist_name',
            'seller_name','company_url','support_url','view_url','artwork_url_large',
            'artwork_url_small','itunes_release_date','copyright','description','version',
            'itunes_version','download_size'],
    "appdetail":['detailapp_id','language_code','detailtitle','detaildescription','release_notes','screenshot_url_1',
                'screenshot_url_2','screenshot_url_3','screenshot_url_4',
                'screenshot_width_height_1','screenshot_width_height_2','screenshot_width_height_3',
                'screenshot_width_height_4','ipad_screenshot_url_1','ipad_screenshot_url_2',
                'ipad_screenshot_url_3','ipad_screenshot_url_4','ipad_screenshot_width_height_1',
                'ipad_screenshot_width_height_2','ipad_screenshot_width_height_3','ipad_screenshot_width_height_4',
                ],
    'genrejoin':[
        'genreapp_id','genre_id','genre_ids',
        ],
    'artistjoin':['artistapp_id','artist_id'
        ],
    'devicejoin':['deviceapp_id','device_type_id'],

}

AppMeta=[
    ['export_date',lambda l:convertticks(l)],
    ['application_id',lambda l:int(l)],
    ['title',lambda l:l],
    ['recommended_age',lambda l:l],
    ['artist_name',lambda l:l],
    ['seller_name',lambda l:l],
    ['company_url',lambda l:l],
    ['support_url',lambda l:l],
    ['view_url',lambda l:l],
    ['artwork_url_large',lambda l:l],
    ['artwork_url_small',lambda l:l],
    ['itunes_release_date',lambda l:date(int(l[0:4]),int(l[5:7]),int(l[8:10]))],
    ['copyright',lambda l:l],
    ['description',lambda l:l],
    ['version',lambda l:l],
    ['itunes_version',lambda l:l],
    ['download_size',lambda l:None if l=="" else int(l)],
    ]

AppDetailMeta=[
    ['exportdate',lambda l:convertticks(l),],
        ['detailapp_id', lambda l:int(l), ],
        ['language_code',lambda l:l,],
        ['detailtitle',lambda l:l,],
        ['detaildescription',lambda l:l,],
        ['release_notes',lambda l:l,],
        ['company_url',lambda l:l,],
        ['support_url',lambda l:l,],
        ['screenshot_url_1',lambda l:l,],
        ['screenshot_url_2',lambda l:l,],
        ['screenshot_url_3',lambda l:l,],
        ['screenshot_url_4',lambda l:l,],
        ['screenshot_width_height_1',lambda l:l,],
        ['screenshot_width_height_2',lambda l:l,],
        ['screenshot_width_height_3',lambda l:l,],
        ['screenshot_width_height_4',lambda l:l,],
        ['ipad_screenshot_url_1',lambda l:l,],
        ['ipad_screenshot_url_2',lambda l:l,],
        ['ipad_screenshot_url_3',lambda l:l,],
        ['ipad_screenshot_url_4',lambda l:l,],
        ['ipad_screenshot_width_height_1',lambda l:l,],
        ['ipad_screenshot_width_height_2',lambda l:l,],
        ['ipad_screenshot_width_height_3',lambda l:l,],
        ['ipad_screenshot_width_height_4',lambda l:l,],
    ]

GenreJoinMetaV1=[
    ['exportdate',lambda l:convertticks(l),],
    ['genre_id',lambda l:int(l), ],
    ['genreapp_id', lambda l:int(l), ],
    ['is_primary', lambda l:bool(int(l)),],
]
GenreJoinMetaV2=[
    ['genreapp_id',lambda l:l,],
    ['genre_id',lambda l:l,],
    ['genre_ids',lambda l:l,],
]


ArtistJoinMeta=[
    ['exportdate',lambda l:convertticks(l),],
    ['artist_id', lambda l:int(l),],
    ['artistapp_id', lambda l:int(l),],
]

DeviceJoinMeta=[
    ['exportdate',lambda l:convertticks(l),],
    ['deviceapp_id', lambda l:int(l), ],
    ['device_type_id', lambda l:int(l),],
]

GenreMeta=[
    ['exportdate',lambda l:convertticks(l),],
    ['genre_id',lambda l:int(l),],
    ['parent_id',lambda l:int(l) if l!='' else 0,],
    ['genrename',lambda l:l]
]




def RowFormatter(inItem, inMeta):
    return Row(**dict(map(lambda k:[k[1][0],k[1][1](k[0])] ,
                zip(inItem,inMeta))))


def loadEPFFile(inFilePath, inMeta=None):
    #if not os.path.exists(inFilePath):
    #    raise IOError("File or folder {} doesn't exist".format(inFilePath))
    if inMeta == None:
        return sc.newAPIHadoopFile(inFilePath, inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",conf={"textinputformat.record.delimiter":chr(2)+"\n",}, keyClass="org.apache.hadoop.io.Text",valueClass="org.apache.hadoop.io.LongWritable").map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l:l.replace(chr(0),'')).map(lambda l: l.split(chr(1))).toDF()
    else :
        return sc.newAPIHadoopFile(inFilePath, inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",conf={"textinputformat.record.delimiter":chr(2)+"\n",}, keyClass="org.apache.hadoop.io.Text",valueClass="org.apache.hadoop.io.LongWritable").map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l:l.replace("\x00","")).map(lambda l: l.split(chr(1))).map(lambda l:RowFormatter(l,inMeta)).toDF()

#sc.newAPIHadoopFile(newapppath+appepfname, inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",conf={"textinputformat.record.delimiter":chr(2)+"\n",}, keyClass="org.apache.hadoop.io.Text",valueClass="org.apache.hadoop.io.LongWritable").map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l: l.split(chr(1))).map(lambda l:map(lambda k:k.replace("\x00",""),l)).map(lambda l:RowFormatter(l,AppMeta)).toDF()



def checkByte(inList):
    for k in inList:
        if type(k)==str or type(k)==unicode:
            if k.find("\x00")!=-1:
                return True
    return False

def replaceByteZeor(inItem):
    if type(inItem)==str or type(inItem)==unicode:
        return inItem.replace('\x00','')
    return inItem

def getLatestDate(inPath):
    files=os.listdir(inPath)
    maxdate=date(1,1,1)
    maxname=""
    for item in files:
        #print item
        if os.path.isdir(os.path.join(inPath,item)):
            match=re.match(r'[\w|_]*(\d{4})(\d{2})(\d{2})[\w|.]*',item)
            #print match
            if match != None :
                #print match.groups()
                itemdate=date(int(match.groups()[0]),int(match.groups()[1]),int(match.groups()[2]))
                if itemdate>maxdate:
                    maxdate=itemdate
                    maxname=item
            #else :
            #    print "not match {}.{}".format(item,match)
    return maxdate,maxname

def getMajorGenre(inGroupBy):
    primary=filter(lambda l:l[2],inGroupBy)
    if len(primary)==0:
        return 0
    else :
        return primary[0][1]

#basepath="/mnt/nfs3/epf/epfdata/"
#newapppath="/mnt/nfs3/epf/epfdata/itunes20150902/"




#if __name__ == "__main__":
parser = argparse.ArgumentParser(description='The tool to convert gzip into json text file')
parser.add_argument('homepath',  nargs="*",
               help='the path where epf file located with full, incremental, latest',default="/mnt/nfs3/epf/epfdata/")
parser.add_argument('--diffdate',action='store',
                    help="the date to be handled",default='')
parser.add_argument('--basedate',action='store',
                        help="the date to be compared",default='')
parser.add_argument('--incrementalpath',action='store',
                        help="the incremental path located",default="incremental")
parser.add_argument('--fullpath',action='store',
                        help="the full path located",default="full")
parser.add_argument('--basepath',action='store',
                            help="the latest baseapp located",default="base")
parser.add_argument('--test',action='store_false',
                        help="the testing mode",default="")
parser.add_argument('--output', action='store',default="",help="The json where gizped files located")
parser.add_argument('--debug', action='store_true')
parser.add_argument('--debugfile', action='store',default='calculateEPF.log',
            help=" the file to store debug logs")
parser.add_argument('--single', action='store_false',help="to avoid duplicate instance")
args = parser.parse_args()
if args.single:
    me = singleton.SingleInstance()
log = logging.getLogger()
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch=logging.StreamHandler()
ch.setFormatter(formatter)
fh=logging.FileHandler(args.debugfile,encoding='utf8')
fh.setFormatter(formatter)
if args.debug:
    ch.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)
else :
    ch.setLevel(logging.INFO)
    fh.setLevel(logging.INFO)
log.addHandler(fh)
log.addHandler(ch)
log.info(args)
if 'sc' not in dir():
    log.info("run in python mode")
    conf = SparkConf().setAppName("calcuate EPF" + args.diffdate).setMaster("spark://10.206.131.44:7077").set("spark.executor.memory","6000M").set("spark.cores.max",8).set("spark.kryoserializer.buffer.max","200M")
    sc = SparkContext(conf=conf)
    print conf.getAll()
sqlContext = SQLContext(sc)
paras={}
homepath=args.homepath
fullpath=homepath+args.fullpath+"/"
incrementalpath=homepath+args.incrementalpath + "/"
basepath=homepath+args.basepath + "/"
basehomepath="/mnt/nfs3/epf/epfdata/base/"
diffhomepath="/mnt/nfs3/epf/epfdata/incremental/"
fullhomepath="/mnt/nfs3/epf/epfdata/full/"
rootdatapath="/mnt/nfs3/epf/epfdata/full/itunes20150902/"
# star the local scripts




if args.basedate!="":
    basedate=date(int(args.basedate[0:4]),int(args.basedate[4:6]),int(args.basedate[6:8]))
    basename="itunes"+basedate.strftime("%Y%m%d")
else:
    basedate,basename=getLatestDate(basepath)

if basename=="":
        raise IOError("can't found app latest date")
        exit(-1)

basepath=basehomepath + basedate.strftime("%Y%m%d") + '/'

if args.diffdate != "":
    diffdate=date(int(args.diffdate[0:4]),int(args.diffdate[4:6]),int(args.diffdate[6:8]))
    diffname="itunes"+diffdate.strftime("%Y%m%d")
else :
    diffdate,diffname=getLatestDate(incrementalpath)

if diffname=="":
    raise IOError("Can't find the diff app latest date")
    exit(-1)


#diffdate=date(2015,9,3)

diffitunespath=diffhomepath + "itunes" + diffdate.strftime("%Y%m%d") +"/"
diffpricepath=diffhomepath + "pricing" + diffdate.strftime("%Y%m%d") +"/"

if args.test:
    diffitunespath='/mnt/nfs3/epf/epfdata/incremental/itunes20150903'
    diffpricepath='/mnt/nfs3/epf/epfdata/incremental/pricing20150903'

resultpath=basehomepath + diffdate.strftime("%Y%m%d")+"/"



#define the names
appepfname="application"
appdetailepfname="application_detail"
genrejoinepfname="genre_application"
artistjoinepfname="artist_application"
devicejoinepfname="application_device_type"
genreepfname="genre"


baseapphistoryname='app_history_parquet/'
baseappname='app_parquet/'
basedetailname='detail_parquet/'
basegenrename='genrejoin_parquet/'
baseartistname='artistjoin_parquet/'
basedevicename='devicejoin_parquet/'
basenodetailname="nodetailapp_parquet/"
basenewchangeapp='newchangeapp_parquet/'

paras['basepath']=basepath
paras['diffitunespath']=diffitunespath
paras['diffpricepath']=diffpricepath
paras['resultpath']=resultpath
paras['rootdatapath']=rootdatapath

log.info( paras )

baseapptable="app_"
baseappinfo="appinfo"
baseshistorytable="apphistory"
jdbcurl = "jdbc:postgresql://10.206.131.1:5432/testapp?user=Administrator&password=mac8.6"



#diffapppath=incrementalpath+"itunes"+diffdate.strftime("%Y%m%d")
log.info("start loading base informtion")

sql = SQLContext(sc)
#Load the base App, Genre information
baseapphistory=sql.read.parquet(os.path.join(basepath,baseapphistoryname))
basedetail=sql.read.parquet(basepath+basedetailname)
baseapp = sql.read.parquet(basepath+baseappname)
if os.path.exists(basepath+basenodetailname):
    basenodetail = sql.read.parquet(basepath+basenodetailname)
else :
    basenodetail = None
basegenre=sql.read.parquet(os.path.join(basepath,basegenrename))
baseartist=sql.read.parquet(os.path.join(basepath,baseartistname))
basedevice=sql.read.parquet(os.path.join(basepath,basedevicename))

baseappids=set(baseapp.map(lambda l:l.application_id).collect())
basedetailids=set(basedetail.map(lambda l:l.detailapp_id).collect())
basegenreids=set(basegenre.map(lambda l:l.genreapp_id).collect())
baseartistids=set(baseartist.map(lambda l:l.artistapp_id).collect())
basedeviceids=set(basedevice.map(lambda l:l.deviceapp_id).collect())
if basenodetail != None :
    basenodetailids=set(basenodetail.map(lambda l:l.nodetailapp_id).collect())
else :
    basenodetailids = set()
log.info("finish loading base informtion")


#origin=loadEPFFile("/mnt/nfs3/epf/epfdata/full/itunes20150902/genre_application",GenreJoinMetaV1).cache()
log.info("start loading diff information")
diffapp = loadEPFFile(os.path.join(diffitunespath,appepfname),inMeta=AppMeta).select(joindict['app'])
log.info("finish loading app diff information")
diffdetail=loadEPFFile(os.path.join(diffitunespath,appdetailepfname),inMeta=AppDetailMeta).select(joindict['appdetail'])
log.info("finish loading detail diff information")
diffgenre=loadEPFFile(os.path.join(diffitunespath,genrejoinepfname),inMeta=GenreJoinMetaV1).map(lambda l:[l.genreapp_id,l.genre_id,l.is_primary]).groupBy(lambda l:l[0]).map(lambda l:[l[0], getMajorGenre(l[1]) ,str(map(lambda k:k[1], sorted(l[1],key=lambda y:y[1])))]).map(lambda l:RowFormatter(l,GenreJoinMetaV2)).toDF()
log.info("finish loading genre diff information")
diffdevice=loadEPFFile(os.path.join(diffitunespath,devicejoinepfname),inMeta=DeviceJoinMeta).map(lambda l:[l.deviceapp_id,l.device_type_id]).groupBy(lambda l:l[0]).map(lambda l:[l[0],sorted(map(lambda y:y[1],l[1].data))]).map(lambda l:Row(deviceapp_id=l[0],device_type_id=str(l[1]))).toDF()
log.info("finish loading device diff information")
diffartist=loadEPFFile(os.path.join(diffitunespath,artistjoinepfname),inMeta=ArtistJoinMeta).select(joindict['artistjoin'])
log.info("finish loading artist diff information")

diffappids=set(diffapp.map(lambda l:l.application_id).collect())
log.info("finish calculate diffappids ")
diffdetailids=set(diffdetail.map(lambda l:l.detailapp_id).collect())
log.info("finish calculate diffdetailids ")
diffgenreids=set(diffgenre.map(lambda l:l.genreapp_id).collect())
log.info("finish getting difffgenreids ")
diffdeviceids=set(diffdevice.map(lambda l:l.deviceapp_id).collect())
log.info("finish getting diffdeviceids ")
diffartistids=set(diffartist.map(lambda l:l.artistapp_id).collect())
log.info("finish getting diffartistids ")

log.info("finish loading diff information")

#calculate changed appids.
log.info("start calcuating changed appids")
changedappids=set(baseapp.join(diffapp,"application_id").filter(baseapp.version!=diffapp.version).map(lambda l:l.application_id).collect())
unchangedappids=baseappids - changedappids
#calcuate new app ids.
log.info("calcuating newappids")
newappids=diffappids-baseappids
newappidsnodetail=newappids-diffdetailids
newappidswithdetail=newappids&diffdetailids



#caculate some inputed detail for no detail app.

newdetailides=basenodetailids&diffdetailids

#calculate the new app items
log.info("calcuate the combinated result app.")
unchangedappidsrdd=sc.parallelize(unchangedappids).map(lambda l:Row(application_id=l)).toDF()
newchangedappidsrdd=sc.parallelize(changedappids|newappids).map(lambda l:Row(application_id=l)).toDF()
#resultapp=baseapp.join(unchangedappidsrdd,"application_id").unionAll(diffapp.join(newchangedappidsrdd,"application_id"))
resultapp1=diffapp.join(newchangedappidsrdd,"application_id")
resultapp2=baseapp.join(unchangedappidsrdd,"application_id")
resultapp=resultapp1.unionAll(resultapp2)



#calcuate new added description.
log.info("merging detail/genre/device/artist")
filterids=basedetailids-diffdetailids
detailfilterrdd=sc.parallelize(filterids).map(lambda l:Row(detailapp_id=l)).toDF()
mergedetail=basedetail.join(detailfilterrdd,"detailapp_id").unionAll(diffdetail)
filterids=basegenreids-diffgenreids
genrefilterrdd=sc.parallelize(filterids).map(lambda l:Row(genreapp_id=l)).toDF()
mergegenre=basegenre.join(genrefilterrdd,"genreapp_id").unionAll(diffgenre)
filterids=basedeviceids-diffdeviceids
devicefilterrdd=sc.parallelize(filterids).map(lambda l:Row(deviceapp_id=l)).toDF()
mergedevice=basedevice.join(devicefilterrdd,"deviceapp_id").unionAll(diffdevice)
filterids=baseartistids-diffartistids
artistfilterrdd=sc.parallelize(filterids).map(lambda l:Row(artistapp_id=l)).toDF()
mergeartist=baseartist.join(artistfilterrdd,"artistapp_id").unionAll(diffartist)



#totalapp=resultapp.join(mergedetail,resultapp.application_id==mergedetail.detailapp_id).join(mergegenre,resultapp.application_id==mergegenre.genreapp_id,"left").join(mergeartist,resultapp.application_id==mergeartist.artistapp_id).join(mergedevice,resultapp.application_id==mergedevice.deviceapp_id).cache()




#cacluate the new and changed app detail items to be inserted into detail table
log.info("calcuate the inserted new app, changed app.s")
insertappids= (newappids|changedappids|basenodetailids | newappidsnodetail ) & diffdetailids
appsub=sc.parallelize(insertappids).map(lambda l:Row(application_id=l)).toDF()
insertapp=resultapp.join(appsub,"application_id")

detailidsrdd=sc.parallelize(insertappids).map(lambda l:Row(detailapp_id=l)).toDF()
detailsub=mergedetail.join(detailidsrdd,"detailapp_id")
detailsub.first()

genreidsrdd=sc.parallelize(insertappids).map(lambda l:Row(genreapp_id=l)).toDF()
genresub=mergegenre.join(genreidsrdd,"genreapp_id")

artistidsrdd=sc.parallelize(insertappids).map(lambda l:Row(artistapp_id=l)).toDF()
artistsub=mergeartist.join(artistidsrdd,"artistapp_id")

deviceidsrdd=sc.parallelize(insertappids).map(lambda l:Row(deviceapp_id=l)).toDF()
devicesub=mergedevice.join(deviceidsrdd,"deviceapp_id")


resultnewchangeappjoin=insertapp.join(detailsub, insertapp.application_id==detailsub.detailapp_id).join(genresub,insertapp.application_id==genresub.genreapp_id,"left").join(devicesub,insertapp.application_id==devicesub.deviceapp_id,"left").join(artistsub,insertapp.application_id==artistsub.artistapp_id,"left").cache()


'''
mergedetailsub=mergedetail.rdd.filter(lambda l:l.detailapp_id in insertappids).toDF().cache()
mergejoin1=insertapp.join(mergedetailsub, insertapp.application_id==mergedetailsub.detailapp_id).cache()
print mergejoin1.count()
mergejoin2=mergejoin1.join(mergegenre,insertapp.application_id==mergegenre.genreapp_id).join(mergedevice,insertapp.application_id==mergedevice.deviceapp_id,"left").cache()
print mergejoin2.count()
resultnewchangeappjoin=mergejoin2.join(mergeartist,insertapp.application_id==mergeartist.artistapp_id,"left").cache()
resultnewchangeappjoin=totalapp.rdd.filter(lambda l:l.application_id in insertappids)
'''

log.info("resultnewchangedappjoin is {}".format(resultnewchangeappjoin.count()))

log.info("{} new app & changed app info inserted into table{}".format(resultnewchangeappjoin.count(),baseshistorytable))
#write the result.
if os.path.exists(resultpath):
    log.info('result path existed')
    #shutil.rmtree(resultpath
    pass

#resultnewchangeappjoin.write.jdbc(jdbcurl,baseshistorytable)


os.makedirs(resultpath)
log.info("start write results")
resultapp.write.parquet(resultpath+baseappname)
resultnewchangeappjoin.write.parquet(resultpath+basenewchangeapp)
log.info("App result writeback done")
mergedetail.write.parquet(resultpath+basedetailname)
log.info("Detail result writeback done")
mergegenre.write.parquet(resultpath+basegenrename)
log.info("Genre result writeback done")
mergedevice.write.parquet(resultpath+basedevicename)
log.info("Device result writeback done")
mergeartist.write.parquet(resultpath+baseartistname)
log.info("Artist result writeback done")
baseapphistory.unionAll(resultnewchangeappjoin).write.parquet(resultpath+baseapphistoryname)
#resultnewchangeappjoin.write.mode('overwrite').jdbc(jdbcurl,baseshistorytable,mode='overwrite')
resultnewchangeappjoin.write.jdbc(jdbcurl,baseshistorytable)
log.info("updated app history information writeback done")
#resultapp.write.jdbc(jdbcurl,baseapptable+diffdate.strftime('%Y%m%d'))
resultapp.write.jdbc(jdbcurl,baseappinfo,mode="overwrite")
log.info("result app write back to database one.")


appwithnodetail=(newappidsnodetail|basenodetailids)-diffdetailids
sc.parallelize(appwithnodetail).map(lambda l:Row(nodetailapp_id=l)).toDF().write.parquet(resultpath+basenodetailname)
log.info("no detail ids parquet updated successful")
log.info("All process {} got finished".format(diffdate.strftime("%Y%m%d")))

writeResult(resultpath+'newappids.csv',newappids)
writeResult(resultpath+'changedappids.csv',changedappids)
log.info('newappids.csv & changedappids.csv writed')

    #Load the latest appinformation, genre information.
        #newapp = loadEPFFile(newapppath+appepfname,inMeta=AppMeta).map(lambda l:map(lambda k:k.replace("\x00","") if type(k)==str or type(k)==unicode else k,l)).cache()
        #newappdetail=loadEPFFile(newapppath+appdetailepfname,inMeta=AppDetailMeta).map(lambda l:map(lambda k:k.replace("\x00","") if type(k)==str or type(k)==unicode else k,l)).cache()
'''
newapp = loadEPFFile(rootdatapath+appepfname,inMeta=AppMeta).select(joindict['app'])
newappdetail=loadEPFFile(rootdatapath+appdetailepfname,inMeta=AppDetailMeta).select(joindict['appdetail'])
genrejoin=loadEPFFile(rootdatapath+genrejoinepfname,inMeta=GenreJoinMetaV1).map(lambda l:[l.genreapp_id,l.genre_id,l.is_primary]).groupBy(lambda l:l[0]).map(lambda l:[l[0], getMajorGenre(l[1]) ,str(map(lambda k:k[1], sorted(l[1],key=lambda y:y[1])))]).map(lambda l:RowFormatter(l,GenreJoinMetaV2)).toDF()
artistjoin=loadEPFFile(rootdatapath+artistjoinepfname,inMeta=ArtistJoinMeta).select(joindict['artistjoin'])
devicejoin=loadEPFFile(rootdatapath+devicejoinepfname,inMeta=DeviceJoinMeta).map(lambda l:[l.deviceapp_id,l.device_type_id]).groupBy(lambda l:l[0]).map(lambda l:[l[0],sorted(map(lambda y:y[1],l[1].data))]).map(lambda l:Row(deviceapp_id=l[0],device_type_id=str(l[1]))).toDF()
genre=loadEPFFile(rootdatapath+genreepfname,GenreMeta)
#oldbaseapp=newapp.join(newappdetail,newapp.application_id==newappdetail.detailapp_id).join(genrejoin,newapp.application_id==genrejoin.genreapp_id,"left").join(devicejoin,newapp.application_id==devicejoin.deviceapp_id,"left").join(artistjoin,newapp.application_id==artistjoin.artistapp_id,"left").drop("detailapp_id").drop("genreapp_id").drop("deviceapp_id").drop("artistapp_id")
baseapp=newapp.join(newappdetail,newapp.application_id==newappdetail.detailapp_id).join(genrejoin,newapp.application_id==genrejoin.genreapp_id,"left").join(devicejoin,newapp.application_id==devicejoin.deviceapp_id,"left").join(artistjoin,newapp.application_id==artistjoin.artistapp_id,"left")
resultpath="/mnt/nfs3/epf/epfdata/base/20150902/"
jdbcurl = "jdbc:postgresql://10.206.131.1:5432/testapp?user=Administrator&password=mac8.6"
baseapp.write.parquet(resultpath+baseapphistoryname)
baseapp.write.jdbc(jdbcurl,baseshistorytable)
newapp.write.jdbc(jdbcurl,baseapptable+basedate.strftime("%Y%m%d"))


'''
