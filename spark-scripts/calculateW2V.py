from pyspark.sql import SQLContext
import json
import re
from stemming.porter2 import stem
from pyspark.mllib.feature import Word2Vec
from pyspark.mllib.linalg import DenseVector
from pyspark.mllib.clustering import KMeans


def clean_word(w):
    return re.sub("\,|\.|\;|\:|\;|\?|\!|\[|\]|\}|\{|\\\u2019|\\\u2026|\W", " ", w.lower())

def clean_word(w):
    return re.sub("\,|\.|\;|\:|\;|\?|\!|\[|\]|\}|\{", " ", w.lower())

def wordToVector (inWord, inModel):
    return inModel.transform(inWord).toArray()




comments=sql.read.load("/Users/Jian_Rong/Projects/spark-test/sample_data/product_comment")
usdrcomments=comments.filter("country_id=1").filter("ProductName='Dr.Cleaner'")
uscomments=comments.filter("country_id=1").select("CommentTitle","CommentContent")
words = uscomments.map(lambda l:clean_word(l[0]+" "+l[1]).split()).map(lambda l: map(lambda k:stem(k), l)).filter(lambda l:len(l)!=0)
word = words.flatMap(lambda l:l)
words_title = uscomments.map(lambda l:(l[0],clean_word(l[0]+" "+ l[1]).split())).filter(lambda l: len(l[1])!=0)#.mapValues(lambda l: " ".join(map(lambda k:stem(k), l)))



word2vec = Word2Vec()
model = word2vec.fit(words)


def wordtransform(inWord,inModel):
    try:
        return inModel.transform(inWord).toArray()
    except ValueError:
        #print "{} not found".format(inWord)
        return np.array([0]*100)




#words_vector=[]
#words_array=[]
#words
#for k in words.collect():
#    words_array.append(map(lambda y: model.transform(y), k))

words_array=[map(lambda y: wordtransform(y,model), k) for k in words.collect()]
words_sumvector=map(lambda j:reduce (lambda x,y: x+y , j) , words_array)
words_vector=[map(lambda k: k/len(n),m) for m, n in zip(words_sumvector, words)]

words_title_array=[[k[0],map(lambda y: wordtransform(y,model), k[1])] for k in words_title.collect()]
words_title_sumvector=map(lambda j:[j[0],reduce (lambda x,y: x+y , j[1])] , words_title_array)
#words_title_vector=[m[0],map(lambda k:k/len(n[1])],m[1]) for m, n in zip(words_title_sumvector, words_title.collect())]
words_title_vector=[[m[0],m[1]/len(n[1])] for m, n in zip(words_title_sumvector, words_title.collect())]
words_title_vectors=sc.parallelize(words_title_vector)

numClusters = 100
numIterations = 25
words_vectors=sc.parallelize(words_vector)
clusters = KMeans.train(words_vectors, numClusters, numIterations)
wssse = clusters.computeCost(words_vectors)

title_membership = words_title_vectors.mapValues(lambda x: clusters.predict(x))
#cluster_centers = map (lambda k: zip(range(0,100),k),clusters.clusterCenters)
#cluster_topics = cluster_centers.mapValues(x => model.findSynonyms(x,5).map(x => x(0)))
map(lambda k: zip(*model.findSynonyms(k,5))[0], clusters.clusterCenters)




-------------------------


def clean_word(w):
     return re.sub("\,|\.|\;|\:|\;|\?|\!|\[|\]|\}|\{", "", stem(w.lower()))

uscomment_bodies=uscomments.map(lambda x: (x['CommentTitle'], " ".join(map(lambda y: clean_word(y), x['CommentContent'].split()))))
uscomment_title=uscomments.map(lambda x:x['title'])
all_terms=uscomment_bodies.map(lambda x:(x[0],list(set(x[1].split()))))
term_document_count=all_terms.flatMap(lambda x:[(i,x[0]) for i in x[1]]).countByKey()
uscomment_bodies=uscomments.map(lambda x: (x[1], " ".join(map(lambda y: clean_word(y), x[2].split()))))
all_terms=uscomment_bodies.map(lambda x:(x[0],list(set(x[1].split()))))
term_document_count=all_terms.flatMap(lambda x:[(i,x[0]) for i in x[1]]).countByKey()
uscomment_bodies=uscomments.map(lambda x: (x[0], " ".join(map(lambda y: clean_word(y), x[1].split()))))



uscomment_bodies=uscomments.map(lambda x: (x[0], " ".join(map(lambda y: clean_word(y), x[1].split()))))
uscomment_title=uscomments.map(lambda x: x[0])
all_terms=uscomment_bodies.map(lambda x: (x[0], list(set(x[1].split()))))
term_document_count = all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]]).countByKey()
all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]])
all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]])
words=all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]])
words.take(5)

uscomment_tf=uscomment_bodies.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).countByValue()
total_documents = 1.0*uscomment_bodies.count()

total_words_per_article = uscomment_bodies.map(lambda x: (x[0],x[1].split())).flatMapValues(lambda x: x).countByValue()
distinct_words_per_article = uscomment_bodies.map(lambda x: (x[0],x[1].split())).flatMap(lambda y: [(y[0],i) for i in y[1]]).distinct()
word_occurences_across_articles = distinct_words_per_article.map(lambda x: (x[1], x[0])).countByKey()

def article_tf_idf(article_total, words_per_article, tf_per_article, occ_across_articles):
    result = []
    for key, value in tf_per_article.items():
        article = key[0]
        term = key[1]
        wpm = words_per_article[article]
        ocm = occ_across_articles[term]
        tf_idf = (float(value)/wpm) * np.log(article_total/ocm)
        result.append({"article":article, "term":term, "score":tf_idf})
    return result


article_word_importance = article_tf_idf(total_documents, total_words_per_article, uscomment_tf, word_occurences_across_articles)


documents_over_time = comments.filter("country_id=1").map(lambda x: ((x[15].year,x[15].month),x['CommentTitle'] )).map(lambda x: (x[0], map(clean_word, x[1].split()))).map(lambda x: (x[0], " ".join(x[1])))

wordbags_over_time = documents_over_time.reduceByKey(lambda x, y: x + " " + y)

tf_per_month = wordbags_over_time.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).countByValue()
tf_per_month.items()[0]

total_words_per_month = wordbags_over_time.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).countByKey()
total_words_per_month.items()[0]

total_months = wordbags_over_time.count()
total_months

distinct_words_per_month = wordbags_over_time.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).distinct()
word_occurences_across_months = distinct_words_per_month.map(lambda x: (x[1], x[0])).countByKey()
word_occurences_across_months.items()[:5]

def monthly_tf_idf(month_total, words_per_month, tf_per_month, occ_across_months):
    result = []
    for key, value in tf_per_month.items():
        month = key[0]
        term = key[1]
        wpm = words_per_month[month]
        ocm = occ_across_months[term]
        tf_idf = (float(value)/wpm) * np.log(month_total/ocm)
        result.append({"month":month, "term":term, "score":tf_idf})
    return result

search_results = monthly_tf_idf(total_months, total_words_per_month, tf_per_month, word_occurences_across_months)

search_results_rdd = sc.parallelize(search_results)


import pandas as pd

def to_ascii(v):
    try:
        return str(v)
    except:
        return ""

def flatten_for_data_frame(r):
    y,m= r['month']
    return {"date":pd.datetime(y,m,1),"term":r['term'],"score":r['score']}

def flatten_for_write(r):
    r['term'] = to_ascii(r['term'])
    return r

import matplotlib as mpl
import matplotlib.pyplot as plt
%matplotlib inline


search_results_df = pd.DataFrame(search_results_rdd.map(flatten_for_data_frame).filter(lambda x: x['term'] in ("flu", "influenza", "rhinovirus", "cold", "china", "palestine")).collect())
search_results_df[:10]

print "articles to compare", search_results_rdd.count()
grouped_terms = search_results_df.sort_index(by=["date"]).groupby("term")
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(16,12));
for term, grp in grouped_terms:
    plt.plot(grp['score'], label=term)
plt.legend(loc="best")

min_date = min(search_results_df['date'])
max_date = max(search_results_df['date'])
mft2 = pd.DataFrame(index=pd.DatetimeIndex(pd.date_range(min_date,max_date,freq="MS")), columns=["date"])
search_history = pd.merge(mft2, search_results_df, left_index=True, right_on="date", how="left")
search_history.index=search_history.date
search_history.index
gt = search_history.groupby("term")
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(16,12));
for t,g in gt:
    g.score.plot(label=t)
plt.legend(loc="best")



slashUStr = "\\u0063\\u0072\\u0069\\u0066\\u0061\\u006E\\u0020\\u5728\\u8DEF\\u4E0A"; #crifan 在路上
decodedUniChars = slashUStr.decode("unicode-escape")
print "decodedUniChars=",decodedUniChars; #decodedUniChars= crifan 在路上
