# !/usr/bin/env python
# -*- coding:utf-8 -*-
from pyspark import SparkConf,SparkContext
conf = SparkConf().setAppName("Load EPF Data").setMaster("spark://10.206.131.32:7077")
sc = SparkContext(conf=conf)
from pyspark.sql import SQLContext,Row
sqlContext = SQLContext(sc)
filebase="/mnt/nfs/itunes20150701/"

filelist=(
    'application', 'application_detail','device_type','genre','application_device_type',
    'genre_application','artist_application' ,'artist' ,
    #'artist_collection',   'collection',
    #'genre_video' ,'mix_type','song_translation','artist_song',
    #'collection_song',
    #'key_value', 'parental_advisory' , 'storefront',
    #'artist_translation'  ,'collection_translation',
    #'media_type', 'role',  'translation_type',
    #'artist_type' , 'collection_type',   'genre_artist',  'mix', 'song', 'video',
    #'artist_video' ,'collection_video'   ,'genre_collection',
    #'mix_collection','song_mix' ,'video_translation'
    )

tables={}
results={}


for item in filelist:
    print "load tablie {} begin".format(item)
    table = sc.newAPIHadoopFile(filebase+item,
                 inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
                 conf={"textinputformat.record.delimiter":chr(2)+"\n",},
                 keyClass="org.apache.hadoop.io.Text",
                 valueClass="org.apache.hadoop.io.LongWritable"
                 ).map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l: l.split(chr(1)))
    #table = sc.newAPIHadoopFile(filebase+item, inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",conf={"textinputformat.record.delimiter":chr(2)+"\n",}, keyClass="org.apache.hadoop.io.Text",valueClass="org.apache.hadoop.io.LongWritable").map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l: l.split(chr(1)))
    

    tableDF=sqlContext.createDataFrame(table)
    tableDF.registerTempTable(item)
    tables[item]=tableDF
    results[item]=tableDF.count()
    print tableDF.first()
    print tableDF.show()
    print "load table {} finish".format(item)

print results




'''
lines = sc.textFile("/mnt/nfs/itunes20150701/application")
#parts = lines.map(lambda l: l.rstrip(chr(2)))
parts = parts.filter(lambda l: l!='')
parts = parts.filter(lambda l: l[0]!='#')
parts2 = parts.map(lambda l: l.split(chr(1)))
print parts2.first()


#application = sc.textFile("/mnt/nfs/itunes20150701/application")
#partapplication = application.map(lambda l: l.rstrip(chr(2)))
#partapplication = partapplication.filter(lambda l: l!='')
#partapplication = partapplication.filter(lambda l: l[0]!='#')
#partsapplication = partapplication.map(lambda l: l.split(chr(1)))
#partsapplication.first()


applicationdetail = sc.textFile("/mnt/nfs/itunes20150701/application_detail")
partapplication = applicationdetail.map(lambda l: l.rstrip(chr(2)))
partapplication = partapplication.filter(lambda l: l!='')
partapplication = partapplication.filter(lambda l: l[0]!='#')
partsapplication = partapplication.map(lambda l: l.split(chr(1)))
print partsapplication.first()
print partsapplication.take(100)
testDF=partsapplication.toDF()
print testDF.take(100)
'''
