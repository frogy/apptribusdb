from pyspark import SQLContext, SparkConf,SparkContext
import codecs
from operator import add
from datetime import datetime,date
import os
import argparse
from tendo import singleton
import logging
from pyspark.sql import Row
from pyspark.sql.functions import *
from pyspark.sql.types import *



def writeResult(inFilePath, inList):
    #fp=codecs.open(inFilePath,mode="wb",encoding="utf-8")
    fp=open(inFilePath,"wb")
    fp.write(codecs.BOM_UTF8)
    for k in inList:
        if type(k) in [list,dict,tuple]:
            for j in k:
                #print j
                #print (str(j)+u',').encode('ascii')
                fp.write((unicode(j)+u',').encode('utf-8'))
        else :
            fp.write(unicode(j).encode('utf-8'))
        fp.write (u'\n')
    fp.close()



#if __name__ == "__main__":
parser = argparse.ArgumentParser(description='The tool to analysis DA Appinfo')
parser.add_argument('filefilter',  nargs="*",
               help='the file name to be analysis, default please input *',default='')
parser.add_argument('--resultpath',action='store',
                    help="the date to be handled",default='/mnt/nfs3/jian/results/')
parser.add_argument('--basepath',action='store',
                    help="the date to be handled",default='/mnt/nfs3/jian/')
parser.add_argument('--test',action='store_false',
                        help="the testing mode",default="")
parser.add_argument('--output', action='store',default="",help="The json where gizped files located")
parser.add_argument('--debug', action='store_true')
parser.add_argument('--debugfile', action='store',default='loganalysisAppInfo.log',
            help=" the file to store debug logs")
parser.add_argument('--singleton', action='store_false',help="to avoid duplicate instance")

args = parser.parse_args()
if args.singleton:
    me = singleton.SingleInstance()
log = logging.getLogger()
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch=logging.StreamHandler()
ch.setFormatter(formatter)
fh=logging.FileHandler(args.debugfile,encoding='utf8')
fh.setFormatter(formatter)
if args.debug:
    ch.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)
else :
    ch.setLevel(logging.INFO)
    fh.setLevel(logging.INFO)
log.addHandler(fh)
log.addHandler(ch)
log.info(args)
if 'sc' not in dir():
    log.info("run in python mode")
    conf = SparkConf().setAppName("calcuate AppInfo" + date.today().strftime("%Y%m%d")).setMaster("spark://10.206.131.44:7077").set("spark.executor.memory","6000M").set("spark.cores.max",8).set("spark.kryoserializer.buffer.max","200M")
    sc = SparkContext(conf=conf)
    print conf.getAll()

sql = SQLContext(sc)
jdbcurl = "jdbc:postgresql://10.206.131.1:5432/testapp?user=Administrator&password=mac8.6"
gettick=udf(lambda l:int((datetime.strptime(l,"%Y-%m-%dT%H:%M:%S.%fZ")-datetime(1970,1,1)).total_seconds()),IntegerType())
uppercol=udf(lambda l:l.upper(),StringType())

#basepath="/Users/Jian_Rong/Jian_Rong/"
basepath=args.basepath
if args.resultpath=='':
    datefolderstr=datetime.now().strftime("%Y%m%d-%H%M%S")+"/"
    resultpath=basepath+datefolderstr
else :
    resultpath=args.resultpath+datetime.now().strftime("%Y%m%d-%H%M%S")+"/"

if not os.path.exists(resultpath):
    os.mkdir(resultpath)

resultdict={}

iplist=sql.read.parquet(basepath+"ip_list_parquet").withColumnRenamed('ip','client_ip')
appdetail=sql.read.parquet(basepath+"mac_app_parquet/").select("bundleId","price","currency").withColumn("bundleId",uppercol("bundleId"))


test=sql.read.json(basepath+'appinfos/'+args.filefilter).withColumn('request_time',gettick('request_time.$date')).rdd.groupBy(lambda l:l.user_UUID+l.app_source).map(lambda l:reduce(lambda u,v:v,sorted(l[1],key=lambda x:x.request_time))).flatMap(lambda l:map(lambda k:Row(app_source=l.app_source,user_UUID=l.user_UUID,client_ip=l.client_ip,**k.asDict()),l.app_infos)).toDF().withColumn("app_bundle_identifier",uppercol("app_bundle_identifier")).join(iplist,"client_ip")
test2=test.join(appdetail,test.app_bundle_identifier==appdetail.bundleId,"left")
test2.write.parquet('/mnt/nfs3/jian/appitems_price_country_parquet/')


exit(-1)


#appinfo=sql.read.json("/mnt/nfs/dc/appinfos/")
#allappinfo=sql.read.json(basepath+'appinfos/'+args.filefilter).withColumn('request_time',gettick('request_time.$date')).groupBy('user_UUID','app_source').max('request_time').cache()
appinfo=allappinfo.join(iplist,"client_ip")
appins=appinfo.filter('app_source="installedAppsChannel"').rdd.groupBy(lambda l:l[6]).map(lambda l:reduce(lambda x,y:x,l[1])).cache()
apprun = appinfo.filter('app_source="runningAppsChannel"').rdd.groupBy(lambda l:l[6]).map(lambda l:reduce(lambda x,y:x,l[1])).cache()
appinsdistinct=appinfo.filter('app_source="installedAppsChannel"').rdd.groupBy(lambda l:l[6]).map(lambda l:reduce(lambda a,b: a,l[1])).cache()





usercount = appinsdistinct.count()
resultdict['totaluser']=usercount
appinsitemcount=appinsdistinct.map(lambda l:[l[6], len(l[2])]).map(lambda l:(l[1],l[0])).countByKey()
appinsitems=appinsdistinct.map(lambda l:[l[6],l[2]]).flatMapValues(lambda l:map(lambda k: k[1], l))

#App installed from store.
appinsitemsinstore = appinsdistinct.map(lambda l:[l[6],l[2]]).flatMapValues(lambda l:map(lambda k: [k[1],k[4]], l)).filter(lambda l:l[1][1]==True).map(lambda l:[l[0],l[1][0]])
appinsitemsin3rd = appinsdistinct.map(lambda l:[l[6],l[2]]).flatMapValues(lambda l:map(lambda k: [k[1],k[4]], l)).filter(lambda l:l[1][1]==False).map(lambda l:[l[0],l[1][0]])
storeappinstallcount=appinsitemsinstore.count()
thirdinstallcount=appinsitemsin3rd.count()
resultdict['TotalStoreAppInstalledNumber']=storeappinstallcount
resultdict['TotalNonStoreAppInstalledNumber']=thirdinstallcount
appinsitemsinstorerank=appinsitemsinstore.groupBy(lambda l:l[1]).map(lambda l: [l[0], len(l[1]),float(len(l[1]))/float(storeappinstallcount)]).sortBy(lambda l:l[1],ascending=False)
appinsitemsin3rdrank=appinsitemsin3rd.groupBy(lambda l:l[1]).map(lambda l: (l[0], len(l[1]),float(len(l[1]))/float(thirdinstallcount))).sortBy(lambda l:l[1],ascending=False)




writeResult(resultpath + "the_rank_of_store_installed_app",appinsitemsinstorerank.collect())
writeResult(resultpath + "the_rank_of_3rd_store_installed_app",appinsitemsin3rdrank.collect())


#write the percent of store vs non store
appfaked=appinsitemsinstorerank.join(appinsitemsin3rdrank).map(lambda l:[l[0],l[1][0],l[1][1],float(l[1][1])/float(l[1][0])]).sortBy(lambda l:l[3],ascending=False)
writeResult(resultpath+"store_vs_non_store_rate.csv",appfaked.collect())

#appins.map(lambda l:[l[1][0],l[2]]).flatMapValues(lambda l:map(lambda k:k[1],l)).groupBy(lambda l:l[1]).countByValue()
appinscountrank=appinsitems.groupBy(lambda l:l[1]).map(lambda l: (l[0], len(l[1]))).sortBy(lambda l:l[1],ascending=False)
appinscountranklist=appinscountrank.map(lambda l:[l[0],l[1],float(l[1]*100)/usercount]).collect()
writeResult(resultpath+"the_rank_of_installed_app_number.csv",appinscountranklist)

runningusercount = apprun.count()
resultdict['TotalNonDistinctRunningUser']=runningusercount
apprunitemcount=apprun.rdd.map(lambda l:[l[6], len(l[2])]).map(lambda l:(l[1],l[0])).countByKey()
apprunitems=apprun.map(lambda l:[l[6],l[2]]).flatMapValues(lambda l:map(lambda k: k[1], l))
#appins.map(lambda l:[l[1][0],l[2]]).flatMapValues(lambda l:map(lambda k:k[1],l)).groupBy(lambda l:l[1]).countByValue()
appruncountrank=apprunitems.groupBy(lambda l:l[1]).map(lambda l: (l[0], len(l[1]))).sortBy(lambda l:l[1],ascending=False)
appruncountranklist=appruncountrank.map(lambda l:[l[0],l[1],float(l[1]*100)/runningusercount]).collect()
writeResult(resultpath+"the_rank_of_running_app_number.csv",appruncountranklist)

runningusercount=apprun.map(lambda l:l[6]).distinct().count()
resultdict['TotalDistinctRunningUser']=runningusercount
apprunitemsinstore = apprun.map(lambda l:[l[6],l[2]]).flatMapValues(lambda l:map(lambda k: [k[1],k[4]], l)).filter(lambda l:l[1][1]==True).map(lambda l:[l[0],l[1][0]])
apprunitemsin3rd = apprun.map(lambda l:[l[6],l[2]]).flatMapValues(lambda l:map(lambda k: [k[1],k[4]], l)).filter(lambda l:l[1][1]==False).map(lambda l:[l[0],l[1][0]])
storeappruncount=apprunitemsinstore.count()
resultdict['TotalStoreAppRunningNumber']=storeappruncount
thirdruncount=apprunitemsin3rd.count()
resultdict['TotalNonStoreAppRunningNumber']=thirdruncount
apprunitemsinstorerank=apprunitemsinstore.groupBy(lambda l:l[1]).map(lambda l: [l[0], len(l[1]),float(len(l[1]))/storeappruncount]).sortBy(lambda l:l[1],ascending=False)
apprunitemsin3rdrank=apprunitemsin3rd.groupBy(lambda l:l[1]).map(lambda l: (l[0], len(l[1]),float(len(l[1]))/thirdruncount)).sortBy(lambda l:l[1],ascending=False)

writeResult(resultpath+"the_rank_of_store_running_app",apprunitemsinstorerank.collect())
writeResult(resultpath+"the_rank_of_3rd_store_running_app",apprunitemsin3rdrank.collect())



exit(-1)



def transPrice(inPrice):
    if inPrice == 0 :
        return "0"
    elif inPrice < 2:
        return "0~2"
    elif inPrice < 4:
        return "2~4"
    elif inPrice <10:
        return "5-10"
    elif inPrice <20:
        return "10~20"
    elif inPrice <30:
        return "20~30"
    elif inPrice < 50:
        return "30~50"
    elif inPrice < 100:
        return "50~100"
    elif inPrice < 200:
        return "100~200"
    elif inPrice < 400:
        return "200~400"
    elif inPrice < 800:
        return "400~800"
    elif inPrice < 1500:
        return "800~1500"
    elif inPrice < 3000:
        return "1500~3000"
    else:
        return ">3000"



curdict={u'TRY':0.3429, u'CHF':1.0390,u'TWD':0.0307,u'RUB': 0.0149,
    u'ZAR':0.0775,u'IDR': 0.0001,u'MXN': 0.0598,u'INR': 0.0153,
     u'JPY': 0.0081,
     u'GBP': 1.5678,
     u'SEK': 0.1177,
     u'HKD': 0.1290,
     u'AED': 0.2722,
     u'EUR': 1.1192,
     u'USD': 1.0,
     u'AUD': 0.7353,
     u'NOK': 0.1215,
     u'DKK': 0.15,
     u'ILS': 0.2581,
     u'NZD': 0.6633,
     u'SAR': 0.2666,
     u'CNY': 0.1566,
     u'SGD': 0.7128,
     u'CAD':0.7654,}


userinsapp=appinsdistinct.map(lambda l:[l[6],l[2]]).flatMapValues(lambda l: l).filter(lambda l:l[1][4]==True).map(lambda l:[l[0],l[1][1].lower()])
appprice = appdetail.map(lambda l:[l.bundleId,l.price,l.currency]).map(lambda l:[l[0].lower(),float(l[2])*curdict[l[1]]])
appprice=appprice.map(lambda l:[l[0],0.0]if l[0].startswith("com.apple.facetime") else l).map(lambda l:[l[0],0.0] if l[0].startswith('com.apple.garageband10') else l)
userpayedmoney=userinsapp.map(lambda l:[l[1],l[0]]).join(appprice).map(lambda l:[l[1][0],l[1][1]]).reduceByKey(add).sortBy(lambda l:l[1],False)
userappwithprice=userinsapp.map(lambda l:(l[1],l[0])).join(appprice).map(lambda l:(l[1][0],l[0],l[1][1])).groupBy(lambda l:l[0]).map(lambda l:(l[0],sorted(l[1],key=lambda k:k[2],reverse=True)))

userpayedmoneywithitems=userpayedmoney.join(userappwithprice).map(lambda l:[l[0],l[1][0],map(lambda k:(k[1],k[2]),l[1][1])]).sortBy(lambda l:l[1],False)

#calculate gross rank
appinsitemsinstorewithprice = appinsitemsinstore.map(lambda l:[l[1],l[0]]).join(appprice).map(lambda l:[l[0],l[1][0],l[1][1]])
storeapprank=appinsitemsinstorewithprice.groupBy(lambda l:l[0]).map(lambda l:[l[0],map(lambda x:x[2],l[1]),len(l[1])]).map(lambda l:[l[0],sum(l[1]),l[2]]).sortBy(lambda l:l[1],ascending=False)
writeResult(resultpath+"the_rank_of_store_app_gross_sale",storeapprank.collect())


#userpayedmoneywithitems.repartition(1).saveAsTextFile(resultpath+"the_payed_money_with_items")


writeResult(basepath+'the_money_user_to_purchase_store_app.csv',userpayedmoney.collect())
writeResult(resultpath+"the_spended_money_with_items.csv",userpayedmoney.join(userappwithprice).map(lambda l:[l[0],l[1][0],map(lambda k:(k[1],k[2]),l[1][1])]).sortBy(lambda l:l[1],False).collect())
writeResult(resultpath+"the_stat_each_price_range.csv",userpayedmoney.map(lambda l:(transPrice(l[1]),1)).reduceByKey(add).collect())

userpayedsoftware=userpayedmoneywithitems.map(lambda l: [l[0],l[1],filter(lambda y:y[1]!=0,l[2])]).map(lambda l: [l[0],l[1],len(l[2]),l[2]])

useravgpaid=userpayedsoftware.filter(lambda l:l[1]!=0).map(lambda l:[l[0],l[1],l[2],l[1]/l[2],l[3]]).sortBy(lambda l:l[3],ascending=False)

writeResult(resultpath+"avg_price_payed_by_user.csv",useravgpaid.collect())

#conver ip
ipinfo=sql.read.parquet(basepath+"ip_list_parquet")
#ipinfo2= ipinfo.filter('_2="try later!"').map(lambda l:l._1).map(lambda l:[l,requests.get('http://10.206.131.29/get_country_code/{}'.format(l)).text])
#ipinfo3=ipinfo.filter('_2!="try later!"').map(lambda l: [l._1,l._2]).union(ipinfo2)
#ipinfowithgeo= ipinfo.filter('_2!="try later!"')

userwithip=appinfo.select("user_UUID","client_ip").filter("user_UUID is not null").filter('user_UUID != ""').map(lambda l:[l.user_UUID,l.client_ip])
userwithgeo=userwithip.map(lambda l:[l[1],l[0]]).join(ipinfowithgeo.rdd).map(lambda l:[l[1][0],l[1][1]]).groupByKey().mapValues(lambda l: reduce(lambda x,y:x,l))
writeResult(resultpath+"user_with_geo.csv",userwithgeo.collect())
appdetailwithgeo=appinsdistinct.map(lambda l:[l[7],l[3]]).flatMapValues(lambda l: l).filter(lambda l:l[1][4]==True).join(userwithgeo).map(lambda l:[l[0],l[1][1]]+list(l[1][0])).map(lambda l:[l[3].lower(),l]).join(appprice).map(lambda l:l[1][0]+[l[1][1]])
paidappwithgeo=appdetailwithgeo.filter(lambda l:l[7]!=0)
freestoreappwithgeo=appdetailwithgeo.filter(lambda l:l[7]==0)

storeappwithgeototal=appdetailwithgeo.count()
paidappwithgeototal=paidappwithgeo.count()
freestoreappwithgeototal=freestoreappwithgeo.count()

storeappcountbycountry=appdetailwithgeo.map(lambda l:[l[1],1]).reduceByKey(add)
paidappcountbycountry=paidappwithgeo.map(lambda l:[l[1],1]).reduceByKey(add)
freestoreappcountbycountry=freestoreappwithgeo.map(lambda l:[l[1],1]).reduceByKey(add)





totalmoney=paidappwithgeo.map(lambda l:[l[1],l[7]]).groupByKey().map(lambda l:[l[0],sum(l[1])]).sortBy(lambda l:l[1]).map(lambda l:l[1]).sum()
totalpaidappcount=paidappwithgeo.count()
countryrevenue=paidappwithgeo.map(lambda l:[l[1],l[7]]).groupByKey().map(lambda l:[l[0],sum(l[1]) ]).map(lambda l:l+[l[1]/totalmoney]).sortBy(lambda l:l[1],ascending=False)
countrypaidappcount=paidappwithgeo.map(lambda l:[l[1],l[7]]).groupByKey().map(lambda l:[l[0],len(l[1])]).map(lambda l:l+[l[1]/totalpaidappcount]).sortBy(lambda l:l[1],ascending=False)
countryusercount=paidappwithgeo.map(lambda l:l[0]+","+l[1]).distinct().map(lambda l:l.split(",")).groupBy(lambda l:l[1]).map(lambda l:[l[0],len(l[1])]).sortBy(lambda l:l[1],ascending=False)
countrydatatemp=countryrevenue.groupWith(countrypaidappcount,countryusercount)
#caculate the top gross app
countrydata=countrydatatemp.map(lambda l:[l[0:1][0]]+map(lambda k:k,l[1][0])+map(lambda m:m, l[1][1])+map(lambda n:n,l[1][2])).map(lambda l:l+[l[1]/totalmoney,l[1]/l[2],l[1]/l[3],float(l[2])/float(totalpaidappcount),float(l[2])/float(l[3])])

countrydata.toDF().write.parquet(resultpath+"countydata")
writeResult(resultpath+"countrydata.csv", countrydata.collect())

paidappbycountry=paidappwithgeo.map(lambda l:[l[1],l[3],l[7]]).groupBy(lambda l:l[0]+"_"+l[1]).map(lambda l: [map(lambda x:[x[0],x[1]],l[1]),len(l[1]),map(lambda y:y[2],l[1])]).map(lambda l:[l[0][0][0],l[0][0][1],l[1],sum(l[2])]).sortBy(lambda l:l[0]+"_"+str(l[3])+str(l[2]),ascending=False)
writeResult(resultpath+"paid_app_by_country.csv",paidappbycountry.collect())



storefreeappbycountry=appdetailwithgeo.filter(lambda l:l[7]==0).map(lambda l:[l[1],l[3],l[7]]).groupBy(lambda l:l[0]+"_"+l[1]).map(lambda l: [map(lambda x:[x[0],x[1]],l[1]),len(l[1]),map(lambda y:y[2],l[1])]).map(lambda l:[l[0][0][0],l[0][0][1],l[1],sum(l[2])]).sortBy(lambda l:l[0]+"_"+str(l[3])+str(l[2]),ascending=False)
writeResult(resultpath+"store_free_app_rank.csv",storefreeappbycountry.collect())

nonstoreappdetailwithgeo=appinsdistinct.map(lambda l:[l[7],l[3]]).flatMapValues(lambda l: l).filter(lambda l:l[1][4]==False).join(userwithgeo).map(lambda l:[l[0],l[1][1]]+list(l[1][0])).map(lambda l:[l[3].lower(),l]).join(appprice).map(lambda l:l[1][0]+[l[1][1]])
nonstoreappbycountry=nonstoreappdetailwithgeo.map(lambda l:[l[1],l[3],l[7]]).groupBy(lambda l:l[0]+"_"+l[1]).map(lambda l: [map(lambda x:[x[0],x[1]],l[1]),len(l[1]),map(lambda y:y[2],l[1])]).map(lambda l:[l[0][0][0],l[0][0][1],l[1],sum(l[2])]).sortBy(lambda l:l[0]+"_"+str(l[3])+str(l[2]),ascending=False)
writeResult(resultpath+"nonstore_app_rank.csv",nonstoreappbycountry.collect())

usercountbycountry=userwithgeo.groupBy(lambda l:l[1]).map(lambda l:[l[0],len(l[1])]).sortBy(lambda l:l[1],ascending=False)
geousertotal=usercountbycountry.map(lambda l:l[1]).reduce(add)
writeResult(resultpath+'user_count_by_country.csv',usercountbycountry.collect())
#geousertotal

#country Total Schema
#0, country_name
#1, totalapp
#2, total storeapp installation
#3: total nonstore app installation
#4: total user
#5  total revenue
#   revneue by country
#   store installation by country
#   non store installation by country






'''
>>> appinfo.printSchema()
root
 |-- __v: long (nullable = true)
 |-- _id: struct (nullable = true)
 |    |-- $oid: string (nullable = true)
 |-- app_infos: array (nullable = true)
 |    |-- element: struct (containsNull = true)
 |    |    |-- _id: struct (nullable = true)
 |    |    |    |-- $oid: string (nullable = true)
 |    |    |-- app_bundle_identifier: string (nullable = true)
 |    |    |-- app_bundle_name: string (nullable = true)
 |    |    |-- app_version: string (nullable = true)
 |    |    |-- is_from_appstore: boolean (nullable = true)
 |-- app_source: string (nullable = true)
 |-- client_ip: string (nullable = true)
 |-- request_time: struct (nullable = true)
 |    |-- $date: string (nullable = true)
 |-- user_UUID: string (nullable = true)



>>> appdetail.printSchema()
root
 |-- _id: struct (nullable = true)
 |    |-- $oid: string (nullable = true)
 |-- appBundle: string (nullable = true)
 |-- appId: string (nullable = true)
 |-- artist: string (nullable = true)
 |-- category: string (nullable = true)
 |-- categoryId: string (nullable = true)
 |-- countryCode: string (nullable = true)
 |-- crawlDate: struct (nullable = true)
 |    |-- $date: long (nullable = true)
 |-- currency: string (nullable = true)
 |-- image53: string (nullable = true)
 |-- link: string (nullable = true)
 |-- name: string (nullable = true)
 |-- price: string (nullable = true)
 |-- rank: long (nullable = true)
 |-- releaseDate: string (nullable = true)
 |-- rights: string (nullable = true)
 |-- searchGenre: long (nullable = true)
 |-- summary: string (nullable = true)
 |-- title: string (nullable = true)
'''
