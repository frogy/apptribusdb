from datetime import datetime, date,timedelta
import os
import sys
from pyspark.sql import SQLContext,Row
import psycopg2
from pyspark import SparkContext, SparkConf
import logging



def getNewUserFileName(inDay):
    return "newuserinfos_{}.json".format(inDay.strftime('%Y-%m-%d'))

def getActiveUserFileName(inDay):
    return 'userinfos_{}.json'.format(inDay.strftime('%Y-%m-%d'))

def isFileExist(inFile):
    return os.path.isfile(inFile)

def isDirExist(inPath):
    return os.path.isdir(inPath)

def getAvaiableFile(inFile):
    print datadirs
    for dirnode in datadirs:
        filepath = dirnode+inFile
        print "filepath is {}".format(filepath)
        if isFileExist(filepath):
            return filepath
    return ''

def registerTable(inSC, inFileName, inTableName):
    print inFileName
    return inSC.load(inFileName,'json')
    #df.registerAsTable(inTableName)

def registerAllTable(indaylist, inSC):
    index=0
    for k in indaylist:
        newusername = getNewUserFileName(k)
        activeusername=getActiveUserFileName(k)
        newuserfile=getAvaiableFile(newusername)
        activeuserfile=getAvaiableFile(activeusername)
        #print "newuser.filename '{}', activeruser.filename '{}'".format(newuserfile,activeuserfile)
        if newuserfile <> '':
            newuserdf.append(registerTable(inSC,newuserfile,'NewUser_D{}'.format(str(index))))
            print "Table NewUser_D{} registered".format(str(index))
        if activeuserfile <> '':
            activeuserdf.append(registerTable(inSC,activeuserfile,'ActiveUser_D{}'.format(str(index))))
            print "Table ActiveUser_D{} registered".format(str(index))
        index+=1

def caclRentionRate(inNewUserDF, inActiveUserDF):
    print 'caculating'
    retentusercount=inNewUserDF.join(inActiveUserDF, inNewUserDF.user_UUID==inActiveUserDF.user_UUID,'inner').select(inNewUserDF.user_UUID).distinct().count()
    totalusercount=inNewUserDF.select('user_UUID').distinct().count()
    print "retentusercount is {}. totalusercount is {}".format(retentusercount,totalusercount)
    return float(retentusercount)/float(totalusercount)

def writeResults(inResults):
    print ("beging write result")
    conn = psycopg2.connect("dbname='PromotionDB_New' user='Administrator' host='10.206.131.1' password='mac8.6'")
    cur=conn.cursor()
    strsql = "INSERT INTO ga_retention_rate (statdate,  retentionday,rate,inserttime)  VALUES (%s, %s, %s, %s) "
    try:
        cur.executemany(strsql,inResults)
        conn.commit()
    except psycopg2.DatabaseError, e:
        print '**********got databaseerror: {}'.format(e.message)
        conn.rollback()
        conn.close()
        return False

    print "write result end"
    return True


def main(args):
    print "args is {}".format(args)
    iDayCount = 60
    if len(args)>0:
        iDayOffset = int(args(0))
    else :
        iDayOffset=1
    firstDate = date(2015,7,2)
    today = date.today()-timedelta(days=iDayOffset)
    daylist=[]
    daynamelist=[]
    results=[]
    print today


    delta = iDayCount if today - firstDate > timedelta(days=iDayCount) else (today - firstDate).days
    print "Time delta is {}".format(delta)
    endDate = today - timedelta(days=delta)
    for dayIndex in range(0,delta+1):
        daylist.append(date.today()-timedelta(iDayOffset+dayIndex))
        daynamelist.append(daylist[dayIndex].strftime('%Y_%m_%d'))
    registerAllTable(daylist,sqlContext)
    print "daylist is {}".format(daylist)
    ActiveUserDF = activeuserdf[0]
    print ActiveUserDF.count()
    for j in range(1, delta+1):
        print "current index is {}".format(j)
        day=daylist[j]
        NewUserDF = newuserdf[j]
        rate = caclRentionRate(NewUserDF, ActiveUserDF)
        results.append((day,j,rate,datetime.now()))
        print "{} {} {}".format ( day, j, rate)

    writeResults(results)


if __name__ == "__main__":
    datadirs=['/home/user/for_retention_rate/','/Users/Jian_Rong/for_retention_rate/','/mnt/nfs/for_retention_rate/']
    newuserdf=[]
    activeuserdf=[]
    conf = SparkConf().setAppName("DrCleaner_Retention_Rate").setMaster("spark://10.206.131.32:7077")

    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.DEBUG
        )
    main(sys.argv[1:])



datetime.strptime(l[12],"%Y-%m-%dT%H:%M:%S.%fZ").strftime("%Y-%m-%dT%H:%M:00")

from pyspark import   SQLContext, Row
sql=SQLContext(sc)
NewUser=sql.load("/mnt/nfs/for_retention_rate/newuserinfos_2015-08-2?.json","json")
test=NewUser.map(lambda l:[requests.get('http://10.206.131.29/get_country_code/{}'.format(l[6][1])).text,datetime.strptime(l[12][0],"%Y-%m-%dT%H:%M:%S.%fZ").strftime("%Y-%m-%dT%H:00:00")])
result2=test.map(lambda l:[l[1],l[0]]).groupByKey().mapValues(len)


from pyspark import SQLContext, Row
from datetime import datetime
import requests

sql=SQLContext(sc)
NewUser=sql.load("/mnt/nfs/for_retention_rate/newuserinfos_2015-08-03.json","json")
NewUserTable=NewUser.map(lambda l:[requests.get('http://10.206.131.29/get_country_code/{}'.format(l[6][1])).text,datetime.strptime(l[12][0],"%Y-%m-%dT%H:%M:%S.%fZ").strftime("%Y-%m-%dT%H:00:00")])
GeoResult=NewUserTable.map(lambda l:[l[1],l[0]]).groupByKey().mapValues(len)
NewUserTable.toDF().registerTempTable("NewUser")
sql.sql("select count(*),_1,_2 from NewUser group by _1, _2").collect()


apppath="/mnt/nfs/itunes20150701/application"
apptable= sc.newAPIHadoopFile(apppath,
             inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
             conf={"textinputformat.record.delimiter":chr(2)+"\n",},
             keyClass="org.apache.hadoop.io.Text",
             valueClass="org.apache.hadoop.io.LongWritable"
             ).map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l: l.split(chr(1)))
apptable.toDF().registerTempTable("app")

appdiffpath="/mnt/nfs/epf/epfdata/incremental/itunes20150801/application"
appdifftable= sc.newAPIHadoopFile(appdiffpath,
             inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
             conf={"textinputformat.record.delimiter":chr(2)+"\n",},
             keyClass="org.apache.hadoop.io.Text",
             valueClass="org.apache.hadoop.io.LongWritable"
             ).map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l: l.split(chr(1)))
appdifftable.toDF().registerTempTable("app_diff")

applicationdetailpath="/mnt/nfs/itunes20150701/application_detail"

appdetailtable = sc.newAPIHadoopFile(applicationdetailpath,
             inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
             conf={"textinputformat.record.delimiter":chr(2)+"\n",},
             keyClass="org.apache.hadoop.io.Text",
             valueClass="org.apache.hadoop.io.LongWritable"
             ).map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l: l.split(chr(1)))
appdetailtable.toDF().registerTempTable("app_detail")


appdetaildiffpath = "/mnt/nfs/epf/epfdata/incremental/itunes20150801/application_detail"
appdetaildifftable = sc.newAPIHadoopFile(appdetaildiffpath,
             inputFormatClass="org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
             conf={"textinputformat.record.delimiter":chr(2)+"\n",},
             keyClass="org.apache.hadoop.io.Text",
             valueClass="org.apache.hadoop.io.LongWritable"
             ).map(lambda l: l[1]).filter(lambda l: l!='').filter(lambda l: l[0]!='#').map(lambda l: l.split(chr(1)))
appdetaildifftable.toDF().registerTempTable("app_detail_diff")


sql=SQLContext(sc)
query = "select * from ga_activeuser"
url = "jdbc:postgresql://10.206.131.1:5432/PromotionDB_New?user=Administrator&password=mac8.6"
gaactiveuser=sql.load(source="jdbc", url=url, dbtable="ga_activeuser")

sql=SQLContext(sc)
url="jdbc:sqlserver://10.206.131.16:1433;user=querier;password=trend#1.."
scriptelement=sql.load(source="jdbc", url=url, dbtable="scriptelement")
scriptelement.write.json("/mnt/nfs/dr/se.json")

scriptelement.write.parquet("/mnt/nfs/dr/scriptelement")



Please instead use:
 - ./spark-submit with --driver-class-path to augment the driver classpath
 - spark.executor.extraClassPath to augment the executor classpath

def getWord(l,inword):
    for k in l:
         if k.find(inword) != -1:
             return True
    return False
