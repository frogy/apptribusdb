from datetime import date,datetime,timedelta
from pyspark import SparkConf,SparkContext
from pyspark.sql import SQLContext,Row
import os
import re
from pyspark import SQLContext
import shutil
import argparse
from tendo import singleton
import logging
import codecs
import shutil



def extractFile(inLocalPath,inFileName,inExtractPath="."):
    if os.path.exists(inLocalPath+inFileName.split(".")[0]):
        log.debug("The file seems existed, please delete the folder {} and retry".format(inLocalPath+inFileName.split(".")[0]))
        return True
    cmd = "cd {} && tar -jvxf ./{} ".format(inLocalPath,inFileName)
    log.info("tar command is {}".format(cmd))
    os.system(cmd)

def rmFolder(inPath):
    if re.search(r"(\d{8})",inPath)==None:
        raise IOExcept("the folder to be deleted isn't correct. Folder is {}".format(inPath))
    shutil.rmtree(inPath)
    log.info("The folder {} got deleted".format(inPath))



def writeResult(inFilePath, inList):
    #fp=codecs.open(inFilePath,mode="wb",encoding="utf-8")
    fp=open(inFilePath,"wb")
    fp.write(codecs.BOM_UTF8)
    for k in inList:
        if type(k)==str or type(k)==unicode:
            fp.write(unicode(j).encode('utf-8'))
        else :
            for j in k:
                #print j
                #print (str(j)+u',').encode('ascii')
                fp.write((unicode(j)+u',').encode('utf-8'))
        fp.write (u'\n')
    fp.close()

def runSpark(inDate,bExtract=True,bDelete=False):
    baseDate=inDate-timedelta(days=1)
    diffDate=inDate
    baseDateStr=baseDate.strftime("%Y%m%d")
    diffDateStr=diffDate.strftime("%Y%m%d")
    log.info("to be handled date is base: {}, diff {}".format(baseDateStr,diffDateStr))
    incrementalpath=args.incrementalpath + "itunes"+diffDateStr
    if bExtract:
        extractFile(args.incrementalpath,"itunes"+diffDateStr+".tbz",args.incrementalpath)
    sparkCmd = "python calculateEPF.py --basedate={} --diffdate={}  ".format(baseDateStr,diffDateStr)
    log.info("spark command is {}".format(sparkCmd))
    r=os.system(sparkCmd)
    if r!=0:
        log.info("running spark met problems, date is {},ret code is {}".format(diffDateStr,str(r)))
        exit(-1)
    log.info("to be deleted path is {}".format(incrementalpath))
    if bDelete:
        rmFolder(incrementalpath)
    log.info("Spark finish handling the date {}".format(diffDateStr))


#if __name__ == "__main__":
parser = argparse.ArgumentParser(description='The tool to run extract EPF files and run spark analysis scripts')
parser.add_argument('dates',  nargs="*",
               help='the date or date list where epf file will be handled',default="")
parser.add_argument('--enddate',action='store',
                    help="the date to be handled",default='')
parser.add_argument('--startdate',action='store',
                        help="the date to be compared",default='')
parser.add_argument('--daynumber',action='store', type=int,
                        help="the date to be compared",default=0)
parser.add_argument('--test',action='store_false',
                        help="the testing mode",default="")
parser.add_argument('--incrementalpath',action='store',
                        help="the incremental path located",default="/mnt/nfs3/epf/epfdata/incremental/")
parser.add_argument('--fullpath',action='store',
                        help="the full path located",default="/mnt/nfs3/epf/epfdata/full/")
parser.add_argument('--basepath',action='store',
                            help="the latest baseapp located",default="/mnt/nfs3/epf/epfdata/base/")
parser.add_argument('--output', action='store',default="",help="The json where gizped files located")
parser.add_argument('--debug', action='store_true')
parser.add_argument('--debugfile', action='store',default='runCalculateEPF.log',
            help=" the file to store debug logs")
parser.add_argument('--singleton', action='store_false',help="to avoid duplicate instance")

args = parser.parse_args()
if args.singleton:
    me = singleton.SingleInstance()
log = logging.getLogger()
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch=logging.StreamHandler()
ch.setFormatter(formatter)
fh=logging.FileHandler(args.debugfile,encoding='utf8')
fh.setFormatter(formatter)
if args.debug:
    ch.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)
else :
    ch.setLevel(logging.INFO)
    fh.setLevel(logging.INFO)
log.addHandler(fh)
log.addHandler(ch)
log.info(args)

startdate=date(int(args.startdate[0:4]),int(args.startdate[4:6]),int(args.startdate[6:8]))
if args.enddate!="" :
    daynumber=(date(int(args.enddate[0:4]),int(args.enddate[4:6]),int(args.enddate[6:8]))-startdate).days
elif args.daynumber !=0:
    daynumber=args.daynumber
else:
    daynumber=1

for i in range(0,daynumber):
    runSpark(startdate+timedelta(days=i),bDelete=True)
