comments=sql.read.load("/Users/Jian_Rong/Projects/spark-test/sample_data/product_comment")
>>> uscomments=comments.filter("contry_id=1")
uscomments=comments.filter("country_id=1").select("CommentTitle","CommentContent")


 def clean_word(w):
     return re.sub("\,|\.|\;|\:|\;|\?|\!|\[|\]|\}|\{\\\u2019\\\u2026", " ", stem(w.lower()))

uscomment_bodies=uscomments.map(lambda x: (x['CommentTitle'], " ".join(map(lambda y: clean_word(y), x['CommentContent'].split()))))
uscomment_title=uscomments.map(lambda x:x['title'])
all_terms=uscomment_bodies.map(lambda x:(x[0],list(set(x[1].split()))))
term_document_count=all_terms.flatMap(lambda x:[(i,x[0]) for i in x[1]]).countByKey()
uscomment_bodies=uscomments.map(lambda x: (x[1], " ".join(map(lambda y: clean_word(y), x[2].split()))))
all_terms=uscomment_bodies.map(lambda x:(x[0],list(set(x[1].split()))))
term_document_count=all_terms.flatMap(lambda x:[(i,x[0]) for i in x[1]]).countByKey()
uscomment_bodies=uscomments.map(lambda x: (x[0], " ".join(map(lambda y: clean_word(y), x[1].split()))))



uscomment_bodies=uscomments.map(lambda x: (x[0], " ".join(map(lambda y: clean_word(y), x[1].split()))))
uscomment_title=uscomments.map(lambda x: x[0])
all_terms=uscomment_bodies.map(lambda x: (x[0], list(set(x[1].split()))))
term_document_count = all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]]).countByKey()
all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]])
all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]])
words=all_terms.flatMap(lambda x: [(i, x[0]) for i in x[1]])
words.take(5)

uscomment_tf=uscomment_bodies.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).countByValue()
total_documents = 1.0*uscomment_bodies.count()

total_words_per_article = uscomment_bodies.map(lambda x: (x[0],x[1].split())).flatMapValues(lambda x: x).countByValue()
distinct_words_per_article = uscomment_bodies.map(lambda x: (x[0],x[1].split())).flatMap(lambda y: [(y[0],i) for i in y[1]]).distinct()
word_occurences_across_articles = distinct_words_per_article.map(lambda x: (x[1], x[0])).countByKey()

def article_tf_idf(article_total, words_per_article, tf_per_article, occ_across_articles):
    result = []
    for key, value in tf_per_article.items():
        article = key[0]
        term = key[1]
        wpm = words_per_article[article]
        ocm = occ_across_articles[term]
        tf_idf = (float(value)/wpm) * np.log(article_total/ocm)
        result.append({"article":article, "term":term, "score":tf_idf})
    return result


article_word_importance = article_tf_idf(total_documents, total_words_per_article, uscomment_tf, word_occurences_across_articles)


documents_over_time = comments.filter("country_id=1").map(lambda x: ((x[15].year,x[15].month),x[4] )).map(lambda x: (x[0], map(clean_word, x[1].split()))).map(lambda x: (x[0], " ".join(x[1])))

wordbags_over_time = documents_over_time.reduceByKey(lambda x, y: x + " " + y)

tf_per_month = wordbags_over_time.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).countByValue()
tf_per_month.items()[0]

total_words_per_month = wordbags_over_time.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).countByKey()
total_words_per_month.items()[0]

total_months = wordbags_over_time.count()


distinct_words_per_month = wordbags_over_time.map(lambda x: (x[0], x[1].split())).flatMapValues(lambda x: x).distinct()
word_occurences_across_months = distinct_words_per_month.map(lambda x: (x[1], x[0])).countByKey()
word_occurences_across_months.items()[:5]

def monthly_tf_idf(month_total, words_per_month, tf_per_month, occ_across_months):
    result = []
    for key, value in tf_per_month.items():
        month = key[0]
        term = key[1]
        wpm = words_per_month[month]
        ocm = occ_across_months[term]
        tf_idf = (float(value)/wpm) * np.log(month_total/ocm)
        result.append({"month":month, "term":term, "score":tf_idf})
    return result

search_results = monthly_tf_idf(total_months, total_words_per_month, tf_per_month, word_occurences_across_months)

search_results_rdd = sc.parallelize(search_results)


import pandas as pd

def to_ascii(v):
    try:
        return str(v)
    except:
        return ""

def flatten_for_data_frame(r):
    y,m= r['month']
    return {"date":pd.datetime(y,m,1),"term":r['term'],"score":r['score']}

def flatten_for_write(r):
    r['term'] = to_ascii(r['term'])
    return r

import matplotlib as mpl
import matplotlib.pyplot as plt
%matplotlib inline


search_results_df = pd.DataFrame(search_results_rdd.map(flatten_for_data_frame).filter(lambda x: x['term'] in ("flu", "influenza", "rhinovirus", "cold", "china", "palestine")).collect())
search_results_df[:10]

print "articles to compare", search_results_rdd.count()
grouped_terms = search_results_df.sort_index(by=["date"]).groupby("term")
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(16,12));
for term, grp in grouped_terms:
    plt.plot(grp['score'], label=term)
plt.legend(loc="best")

min_date = min(search_results_df['date'])
max_date = max(search_results_df['date'])
mft2 = pd.DataFrame(index=pd.DatetimeIndex(pd.date_range(min_date,max_date,freq="MS")), columns=["date"])
search_history = pd.merge(mft2, search_results_df, left_index=True, right_on="date", how="left")
search_history.index=search_history.date
search_history.index
gt = search_history.groupby("term")
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(16,12));
for t,g in gt:
    g.score.plot(label=t)
plt.legend(loc="best")
