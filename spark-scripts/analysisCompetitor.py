from pyspark.sql import SQLContext
import json
import re
from stemming.porter2 import stem
from pyspark.mllib.feature import Word2Vec
from pyspark.mllib.linalg import DenseVector
from pyspark.mllib.clustering import KMeans
import numpy as np


def clean_word(w):
    return re.sub("\,|\.|\;|\:|\;|\?|\!|\[|\]|\}|\{|\\\u2019|\\\u2026|\W", " ", w.lower())

def wordtransform(inWord,inModel):
    try:
        return inModel.transform(inWord).toArray()
    except ValueError:
        #print "{} not found".format(inWord)
        return np.array([0]*100)

sql = SQLContext(sc)
detailsdf=sql.read.load("/mnt/nfs2/jian/appdetail.json","json")
details=detailsdf.filter("resultCount<>0")
items=details.select("results.trackId","results.trackName","results.description")
words=items.rdd.map(lambda l:clean_word(l[1][0] + " " + l[2][0]).split()).map(lambda l: map(lambda k: stem(k), l)).filter(lambda l:len(l)!=0)
words_title=items.rdd.map(lambda l:(l[0],clean_word(l[1][0] + " " + l[2][0]).split())).map(lambda l: (l[0],map(lambda k: stem(k), l[1]))).filter(lambda l:len(l[1])!=0)

word2vec = Word2Vec()
model = word2vec.fit(words)

#New method
worddistinct= worddistinct=words.flatMap(lambda l: l).distinct()
words_array_index= {k:wordtransform(k,model)) for k in worddistinct.collect()}
words_index=sc.parallelize(words_array_index)


words_array=words.map(lambda l: map(lambda y: words_array_index[y],l))
#words_sumvector=words_array.map(lambda j:reduce (lambda x,y: x+y , j))
words_vectors=words_array.map(lambda j:reduce (lambda x,y: x+y , j)/len(j))
#words_vector=[map(lambda k: k/len(n),m) for m, n in zip(words_sumvector, words)]
#words_vectors=sc.parallelize(words_vector)


words_title_vectors=words_title.map(lambda l: [l[0],map(lambda y:words_array_index[y],l[1]),len(l[1])]).map(lambda l:[l[0],reduce(lambda x,y: x+y, l[1]),l[2]]).map(lambda l:[l[0],l[1]/l[2]])





'''
words_title_array=[[k[0],map(lambda y: wordtransform(y,model), k[1])] for k in words_title.collect()]
words_title_sumvector=map(lambda j:[j[0],reduce (lambda x,y: x+y , j[1])] , words_title_array)
#words_title_vector=[m[0],map(lambda k:k/len(n[1])],m[1]) for m, n in zip(words_title_sumvector, words_title.collect())]
words_title_vector=[[m[0],m[1]/len(n[1])] for m, n in zip(words_title_sumvector, words_title.collect())]
words_title_vectors=sc.parallelize(words_title_vector)
'''

numClusters = 300
numIterations = 20

clusters = KMeans.train(words_vectors, numClusters, numIterations)
wssse = clusters.computeCost(words_vectors)

title_membership = words_title_vectors.mapValues(lambda x: clusters.predict(x))

results=title_membership.sortBy(lambda l:l[1]).join(items.map(lambda l:[l[0],l[1]]))


results =sql.read.json("/mnt/nfs2/jian/the_product_cluster_results.json/")
#cluster_centers = map (lambda k: zip(range(0,100),k),clusters.clusterCenters)
#cluster_topics = cluster_centers.mapValues(x => model.findSynonyms(x,5).map(x => x(0)))
map(lambda k: zip(*model.findSynonyms(k,5))[0], clusters.clusterCenters)
