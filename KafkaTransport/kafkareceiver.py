#!/usr/bin/env python
import threading, logging, time
import argparse
from kafka.client import KafkaClient
from kafka.consumer import SimpleConsumer
from kafka.producer import SimpleProducer
import json
from bson.py3compat import PY3, u, text_type, iteritems, StringIO
import re

if __name__ == "__main__":
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)

    ch=logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)


    parser = argparse.ArgumentParser(description='The tool to receive testing appid message from Kafka')
    parser.add_argument('--outputfile',action='store',
                    help=" use file as output",default="")
    parser.add_argument('--messageid',action='store',default='monitorappidtest',
                    help=" override the default messageid in kafka")
    parser.add_argument('--groupid',action='store',default='defaultgroup',
                    help=" override the default groupid in kafka")
    parser.add_argument('--host',action='store',default='10.206.131.30',
                    help=" override the default host address. host name or ip address ")
    parser.add_argument('--port',action='store',metavar='N',type=int,nargs='?',
                    help=" overide the default kafka server port.",default=9092)
    parser.add_argument('--repeat', action='store_true')
    parser.add_argument('--interval',metavar='N',type=int,nargs='?',
                    help=" the interval to request new messageid ")
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--debugfile', action='store',default='receiver.log',
                    help=" the file to store debug logs")

    args = parser.parse_args()
    fh=logging.FileHandler(args.debugfile,encoding='utf8')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    log.addHandler(fh)
    log.info(args)
    count=0
    #start receiver.
    client = KafkaClient("{}:{}".format(args.host,args.port))
    consumer = SimpleConsumer(client, args.groupid, args.messageid)
    count =0


    messages = consumer.get_messages()
    for message in messages:
        count +=1
        log.info(message.message.value)
    log.info("1st batch got {} messages".format(count))
    if args.repeat:
        for message in consumer:
            count += 1
            print(message.message.value)
            print "total message is %s" % (count)
            #print dir(consumer)
            #consumer.commit(message)
    if args.outputfile!="":
        hf.close()
