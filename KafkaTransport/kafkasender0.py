#!/usr/bin/env python
import argparse
import threading, logging, time

from kafka.client import KafkaClient
from kafka.consumer import SimpleConsumer
from kafka.producer import SimpleProducer
import json
from bson.py3compat import PY3, u, text_type, iteritems, StringIO
import re





if __name__ == "__main__":
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)

    ch=logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)


    parser = argparse.ArgumentParser(description='The tool to send appid to Kafka')
    parser.add_argument('ids', metavar='N', type=int, nargs='*',
                   help='ids to be sent')
    parser.add_argument('--id', metavar='N', type=int, nargs='?',
                    action='store',
                    help='the id to be sent')
    parser.add_argument('--file',action='store_true',
                    help=" use file as input")
    parser.add_argument('--messageid',action='store',default='newappids',
                    help=" override the default messageid in kafka")
    parser.add_argument('--host',action='store',default='blog.ianrong.com',
                    help=" override the default host address. host name or ip address ")
    parser.add_argument('--port',action='store',metavar='N',type=int,nargs='?',
                    help=" overide the default kafka server port.",default=80)
    parser.add_argument('--inputfile',action='store',
                    help=" the file to input app ids. ",default="")
    parser.add_argument('--interval',metavar='N',type=int,nargs='?',
                    help=" the interval to sent multiple ")
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--debugfile', action='store',default='send.log',
                    help=" the file to store debug logs")

    args = parser.parse_args()
    fh=logging.FileHandler(args.debugfile,encoding='utf8')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    log.addHandler(fh)

    log.info(args)
    if args.id==None and len(args.ids)==0 and args.inputfile=="":
        log.error("Please choose one input method, id/ids/inputfile")
        exit(-1)
    if args.inputfile!="":
        hf=open(args.inputfile,'r')
        inputids=hf.readlines()
    else :
        if args.id==None:
            args.id=[]
        else :
            args.id=list(args.id)
        inputids=args.id+args.ids

    count=0
    client = KafkaClient("{}:{}".format(args.host,args.port))
    producer = SimpleProducer(client)
    count=len(inputids)
    ids={"appIDs":inputids}
    #for item in inputids:
    #    producer.send_messages(args.messageid, str(item))
    #    count += 1
    log.info("will send json {}".format(json.dumps(ids)))
    producer.send_messages(args.messageid, json.dumps(ids))
    log.info("send total {} message".format(count))
    #producer.close()
    if args.inputfile!="":
        hf.close()
