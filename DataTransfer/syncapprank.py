import requests
import re
import time
import os
import hashlib
from requests.exceptions import *
from tendo import singleton
import getopt
import sys
import logging
#import paramiko
import argparse
from datetime import date,datetime

def getS3File(inRemotePath,inLocalPath, inFileName):
    cmd = "s3cmd get {}{} {}{}".format(inRemotePath,inFileName, inLocalPath,inFileName)
    log.info("the command to get s3 file is"+cmd)
    os.system(cmd)
    return

def verifyFileSize(inFilePath,inSize):
    if not isFileExist(inFilePath):
        return False
    actsize = os.path.getsize(inFilePath)
    log.debug("The actual size of file {} is {}".format(inFilePath,inSize))
    if actsize == inSize:
        return True
    else :
        return False

def getFileList(inS3Path):
    r=os.popen("s3cmd ls {}".format(inS3Path))
    filetemplist=[]
    for line in r.readlines():
        #filedate,filetime,filesize,filename=line.split()
        #log.DEBUG ("{}'s size is {}".format(filename,filesize))
        filetemplist.append(line.split())
    r.close()
    filelist={}
    for item in filetemplist:
        if item[0]=='DIR':
            continue
        filedate,filetime,filesize,filename=item
        r=re.search("(\d{4}-\d{2}-\d{2}.tar)$",filename)
        if r:
            filename = r.group(0)
            filelist[filename]={"size":int(filesize),"date":datetime.strptime(filename,"%Y-%m-%d.tar").date()}
            #log.debug ("{}'s size is {}".format(filename,filesize))
    log.debug("the filelist dict is {}".format(filelist))
    return filelist

def getMaxFileName(inFileDict):
    filename=None
    maxdate=date(1970,1,1)
    for item in inFileDict.items():
        if ( item[1]["date"] > maxdate):
            filename=item[0]
            maxdate=item[1]["date"]
    return filename

def isFileExist(inFilePath):
    return os.path.exists(inFilePath)

def extractFile(inRemotePath,inLocalPath,inFileName):
    cmd = "cd {} && tar -vxf {}{} .".format(inLocalPath,inLocalPath,inFileName)
    log.debug("tar command is {}".format(cmd))
    os.system(cmd)

def downloadS3File(inS3Path,inLocalPath,inFileName=""):
    fl=getFileList(inS3Path)
    if inFileName=="":
        fn=getMaxFileName(fl)
    else :
        fn=inFileName
    while not verifyFileSize(inLocalPath+fn,fl[fn]["size"]):
        if isFileExist(inLocalPath+fn):
            os.remove(inLocalPath+fn)
        log.debug("begin to get S3 File {}".format(fn))
        getS3File(inS3Path,inLocalPath,fn)
        time.sleep(10)
    log.info("the file {} is already downloaded".format(fn))
    return True



if __name__ == "__main__":
    me = singleton.SingleInstance()
    parser = argparse.ArgumentParser(description='The tool to send appid to Kafka')
    parser.add_argument('filenames', metavar='N', nargs='*',
                   help='filename to be downloaded')
    '''
    parser.add_argument('--id', metavar='N', type=int, nargs='?',
                    action='store',
                    help='the id to be sent')
    parser.add_argument('--file',action='store_true',
                    help=" use file as input")
    parser.add_argument('--file',action='store', default="s3://iosdb_rawdata/"
                    help=" use file as input")
    parser.add_argument('--messageid',action='store',default='monitorappidtest',
                    help=" override the default messageid in kafka")
    '''
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--all', action='store_true')
    parser.add_argument('--extract', action='store_true')
    parser.add_argument('--debugfile', action='store',default='syncapprank.log',
                help=" the file to store debug logs")
    parser.add_argument('--bucket', action='store',default='s3://iosdb_rawdata/',
                    help=" the S3 file bucket name and path")

    args = parser.parse_args()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch=logging.StreamHandler()
    ch.setFormatter(formatter)
    fh=logging.FileHandler(args.debugfile,encoding='utf8')
    fh.setFormatter(formatter)
    if args.debug:
        ch.setLevel(logging.DEBUG)
        fh.setLevel(logging.DEBUG)
    else :
        ch.setLevel(logging.INFO)
        fh.setLevel(logging.INFO)
    log.addHandler(fh)
    log.addHandler(ch)
    log.info(args)

    if len(args.filenames)==0 and not args.all:
            downloadS3File(args.bucket,os.getcwd()+"/data/")
            if args.extract:
                extractFile(args.bucket,os.getcwd()+"/data/",os.getcwd()+"/data/",getMaxFileName(getFileList(args.bucket)))
    else :
        if args.all:
            args.filenames=getFileList(args.bucket).keys()
        for item in args.filenames:
            downloadS3File(args.bucket,os.getcwd()+"/data/",item)
            if args.extract:
                extractFile(args.bucket,os.getcwd()+"/data/",item)
    log.info("finish downloading. exit()")
