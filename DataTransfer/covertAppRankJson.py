import os,gzip,zlib
from tendo import singleton
import argparse
import logging
import re
import json

def getLocalJsonList(inPath):
    files=os.listdir(inPath)
    print files
    jsonfiles=[]
    for item in files:
        log.debug('got item {}'.format(item))
        if os.path.isfile(inPath+item) and item.find('.json')!=-1:
            jsonfiles.append(item)
    return jsonfiles

def isGZipString(inSrc):
    if inSrc[0:4]=='\x1F\xC2\x8B\x08':
        return True
    else:
        return False


def convertToJson(inFilePath,inDestPath=""):
    filename=inFilePath.rsplit("/",1)[1]
    if not os.path.exists(inFilePath):
        log.error("The file {} doens't exist".format(inFilePath))
        return False
    fh=open(inFilePath,'rb')
    src=fh.read()
    fh.close()
    if isGZipString(src):
        dest=zlib.decompress(src.decode('utf8').encode('ISO-8859-1'),15+32)
    else :
        log.debug("The file {} is already json".format(inFilePath))
        return True

    if inDestPath=="":
        fresult=open(inFilePath,"w")
    else :
        if not os.path.exsits(inDestPath):
            os.mkdir(inDestPath)
        fresult=open(inDestPath+filename,'wb')
    fresult.write(dest)
    fresult.flush()
    fresult.close()

    return True

def makeRankJson(inJsonPath,inRankPath):
    filename=inJsonPath.rsplit("/",1)[1]
    country,genre,feed=re.match(r'iosdb_rank-(\w{2})-([\w]{3,4})-(\w*).json',filename).groups()
    f=open(inJsonPath,"r")
    rank=json.load(f)
    f.close()
    fresult=open(inRankPath,'a')
    if not rank['feed'].has_key('entry'):
        log.error(" covert json file {} got error: no data times".format(inJsonPath))
        return False
    items=rank['feed']['entry']
    rank=0
    for item in items:
        rank+=1
        appid=int(item['id']['attributes']['im:id'])
        bundleId=item['id']['attributes']['im:bundleId']
        title=item['title']['label']
        genrename=item['category']['attributes']['term']
        currency=item['im:price']['attributes']['currency']
        price=float(item['im:price']['attributes']['amount'])
        itemdict={
            'country':country,
            'genre':genre,
            'feed':feed,
            'rank':rank,
            'appid':appid,
            'bundleId':bundleId,
            'title':title,
            'genrename':genrename,
            'currency':currency,
            'price':price,
        }
        jsonstr=json.dumps(itemdict)
        fresult.write(jsonstr+'\n')
    fresult.close()
    return True






if __name__ == "__main__":
    me = singleton.SingleInstance()
    parser = argparse.ArgumentParser(description='The tool to convert gzip into json text file')
    parser.add_argument('inputpath',  nargs=1,
                   help='the path where gzip file located')
    parser.add_argument('--jsonfilename',action='store',
                        help="the json file to store",default='ranks.result')
    '''
    parser.add_argument('--jsonfile', metavar='N', type=int, nargs='?',
                    action='store',
                    help='the id to be sent')

    parser.add_argument('--file',action='store', default="s3://iosdb_rawdata/"
                    help=" use file as input")
    parser.add_argument('--messageid',action='store',default='monitorappidtest',
                    help=" override the default messageid in kafka")
    '''
    parser.add_argument('--output', action='store',default="",help="The json where gizped files located")

    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--debugfile', action='store',default='convertAppRankJson.log',
                help=" the file to store debug logs")

    args = parser.parse_args()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch=logging.StreamHandler()
    ch.setFormatter(formatter)
    fh=logging.FileHandler(args.debugfile,encoding='utf8')
    fh.setFormatter(formatter)
    if args.debug:
        ch.setLevel(logging.DEBUG)
        fh.setLevel(logging.DEBUG)
    else :
        ch.setLevel(logging.INFO)
        fh.setLevel(logging.INFO)
    log.addHandler(fh)
    log.addHandler(ch)
    log.info(args)

    localpath=args.inputpath[0]
    files=getLocalJsonList(localpath)
    log.info("path {} get total {} json files".format(localpath,len(files)))
    count=0
    success=0
    jsonsuccess=0
    fail=0
    for item in files:
        count+=1
        log.info("covert #{} file {} to json file".format(count,item))
        if convertToJson(localpath+item):
            success+=1
            if makeRankJson(localpath+item,localpath+args.jsonfilename):
                jsonsuccess+=1
        else :
            fail+=1

    log.info("total {} json files, success {}, failed {}.Rank Json got {}".format(len(files),success,fail,jsonsuccess))
