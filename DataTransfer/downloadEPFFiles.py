
import requests
import re
import time
import os
import hashlib
from requests.exceptions import *
from tendo import singleton
import getopt
import sys
import logging
from datetime import date,datetime


def scpfile(inFile,inRemotePath,inLocalPath):
    if config['usescpkey']:
        scpcmd = "scp -i {} {}@{}:{} {}".format(config['scpkey'], config['scpuser'],config['scphost'],inRemotePath+inFile, inLocalPath+inFile)
    else:
        scpcmd = "scp  {}@{}:{} {}".format(config['scpuser'],config['scphost'],inRemotePath+inFile, inLocalPath+inFile)
    log.info( "scp download cmd is {} ".format(scpcmd))
    result = os.system(scpcmd)
    log.debug( "scp result is {}".format(result))
    return result

def scpRemoveFile(inFileName,inRemotePath):
    log.info( "The file {} is already download, will remove remote file {}.".format(inFileName,inRemotePath+inFileName))

    s = paramiko.SSHClient()
    s.load_system_host_keys()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if config['usescpkey']:
        key = paramiko.RSAKey.from_private_key_file(config['scpkey'])
        s.connect(config['scphost'],22,config['scpuser'],pkey=key)
    else :
        s.connect(config['scphost'],22,config['scpuser'])
    cmd =" rm {}{}".format(inRemotePath,inFileName)
    log.info( "scpRemoveFile-->Remove File Command is {}".format(cmd))
    stdin,stdout,stderr = s.exec_command(cmd)
    for std in stdout.readlines():
        log.info( std )
    s.close()

def downloadFile(inUrl,inDownloadPath):
    local_filename = inUrl.split('/')[-1]
    # NOTE the stream=True parameter

    try:
        r = requests.get(inUrl, stream=True, auth=(config['user'],config['password']))
        with open(inDownloadPath+local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
                    f.flush()
    except  ConnectionError, e:
        log.error( "download file {} got exception {}".format(local_filename,e.message) )
    except  (HTTPError, Timeout , TooManyRedirects), e:
    #r =requests.post(self.submitURL,json.loads(data), verify=False)
        log.error( "download file {} got exception {}".format(local_filename,e.message) )
    return local_filename

def getFileMD5(filepath):
    f=open(filepath,'r')
    m=hashlib.md5()
    block = f.read(128)
    while block!="":
        m.update(block)
        block=f.read(128)
    return m.hexdigest()

def getFullDateList():
    for cycle in range(0,5):
        try:
            r=requests.get(config['baseurl'],auth=(config['user'],config['password']))
        except  ConnectionError :
            print "Request EPF main folder  {} got exception".format(inResult[1])
            return ""
        except  (HTTPError, Timeout , TooManyRedirects):
        #r =requests.post(self.submitURL,json.loads(data), verify=False)
            print "Request EPF main folder {} got exception".format(inResult[1])
            return ""
        if r.status_code==200:
            log.info('getFullDate->get url {} resutl is {}, text is {}'.format(config['baseurl'],r.status_code,r.text))
            datelist=re.findall(r'href="(\d{8})/"', r.text)
            return datelist
    return []

def getFullDate(inoffset=0):
    datelist = getFullDateList()
    if datelist == [] or len(datelist)<inoffset:
        return ""
    else :
        return datelist[-1-inoffset]

def getFullUrlPath(inFullDate="",inOffset=0):
    if inFullDate != "" :
        datestr=inFullDate
    else:
        datestr=getFullDate(inOffset)
    if datestr=="":
        return ""
    else :
        return config['baseurl'] + datestr + "/"


def getIncrementalDateList(inFullDate="",inFullOffset=0 ):
    datestrlist=getFullDateList()
    log.debug("the full datelist is {}".format(str(datestrlist)))
    if inFullDate!="":
        datestrlist=filter(lambda l:l==inFullDate,datestrlist)
    if inFullOffset!=0:
        datestrlist=datestrlist[inFullOffset:inFullOffset+1]
    datelist=map(lambda l:date(int(l[0:4]),int(l[4:6]),int(l[6:8])),datestrlist)
    datelist.sort(reverse=True)
    topdatelist=datelist[0:5]
    incdatelist=[]
    log.debug("getIncrementalDateList will check the folllowing datelist {}".format(str(topdatelist)))
    for dateitem in topdatelist:
        tempurl=config['baseurl']+dateitem.strftime("%Y%m%d")+"/incremental/"
        log.debug("getIncrementalDateList will retrive the ulr {}".format(tempurl))
        for cycle in range(0,5):
            try:
                r=requests.get(tempurl,auth=(config['user'],config['password']))
            except  ConnectionError ,e:
                print "Request EPF main folder  {} got exception {}".format(inResult[1],e.message)
                return ""
            except  (HTTPError, Timeout , TooManyRedirects),e:
            #r =requests.post(self.submitURL,json.loads(data), verify=False)
                print "Request EPF incremental folder {} got exception {}".format(inResult[1],e.message)
                return ""
            if r.status_code==200:
                log.debug(r.text)
                tempincdatelist=map(lambda l:[date(int(l[0:4]),int(l[4:6]),int(l[6:8])),l,dateitem.strftime("%Y%m%d")],re.findall(r'>(\d{8})/<', r.text))
                incdatelist+=tempincdatelist
                break
    if incdatelist != []:
        incdatelist.sort(cmp=lambda x,y:cmp(x[0],y[0]),reverse=True)
    log.info("getIncrementalDateList return the datelist {}".format(str(incdatelist)))
    return incdatelist

def extractFile(inLocalPath,inFileName,inExtractPath="."):
    if os.path.exists(inLocalPath+inFileName.split(".")[0]):
        log.debug("The file seems existed, please delete the folder {} and retry".format(inLocalPath+inFileName.split(".")[0]))
        return True
    cmd = "cd {} && tar -jvxf ./{} {}".format(inLocalPath,inFileName,inExtractPath)
    log.debug("tar command is {}".format(cmd))
    os.system(cmd)

def getIncrementalUrlPath(inFullDate="",inIncrementalDate="",inFullOffset=0,inIncrementalOffset=0):
    incdatelist=getIncrementalDateList(inFullDate,inFullOffset)
    if inIncrementalDate!="":
        incdatelist=filter(lambda l:l[1]==inIncrementalDate,incdatelist)
    log.info("getIncrementalUrlPath will return {}".format(config['baseurl']+ incdatelist[inIncrementalOffset][2] + "/incremental/" + incdatelist[inIncrementalOffset][1] + "/"))
    return config['baseurl']+ incdatelist[inIncrementalOffset][2] + "/incremental/" + incdatelist[inIncrementalOffset][1] + "/"

def getFileList(inUrl,inCycle=5):
    for cycle in range(0,inCycle):
        try:
            r=requests.get(inUrl,auth=(config['user'],config['password']))
        except  ConnectionError :
            log.info( "Request EPF main folder  {} got exception".format(inResult[1]))
        except  (HTTPError, Timeout , TooManyRedirects):
        #r =requests.post(self.submitURL,json.loads(data), verify=False)
            log.info( "Request EPF main folder {} got exception".format(inResult[1]) )
        if r.status_code == 200:
            filelist=re.findall(r'href="(\w+[.\w]+)"', r.text)
            break
    if len(filelist)==0:
        log.error("file list is empty")
    log.info("file list is "+ str(filelist))
    return filelist

def getFileMd5List(inUrl,inFiles, inCycle=5):
    md5list={}
    for k in inFiles:
        if k.find('md5')>0:
            md5result=False
            md5file = k.rstrip('.md5')
            md5Count=0
            log.info( k.rstrip(".md5") )

            while (not md5result) and md5Count<inCycle :
                try :
                    md5r=requests.get(inUrl+k,auth=(config['user'],config['password']))
                except  ConnectionError :
                    log.error( "Request EPF main folder  {} got exception".format(inResult[1]) )
                    md5Count +=1
                    continue
                except  (HTTPError, Timeout , TooManyRedirects):
                #r =requests.post(self.submitURL,json.loads(data), verify=False)
                    log.error( "Request EPF main folder {} got exception".format(inResult[1]) )
                    md5Count+=1
                    continue

                md5Count += 1
                if md5r.status_code == 200 :
                    md5text = md5r.text
                    log.info("file {}'s md5 is {}'".format(k, md5text))
                    temp=re.findall(r'=\s(\w+)\n',md5text)
                    md5list[md5file]=temp[0]
                    md5result=True
                else :
                    time.sleep(5)
                    md5Count += 1

    log.info("md5 file list is " + str(md5list))
    return md5list

def verifyFileMD5(inFilePath):
    filename=inFilePath.rstrip("/",1)[1]
    expectMD5=MD5(filename)
    actualMD5=getFileMD5(inFilePath)
    log.debug("The file expected md5 is {}, actual is {}".format(expectedMD5, actualMD5))
    if expectMD5 == actualMD5:
        return True
    else :
        return False


def scpFiles(inUrl,inRemotePath,inLocalPath,inCycle=5):
    Files=getFileList(inUrl,inCycle)
    MD5 = getFileMd5List(inUrl, Files, inCycle)

    for k in Files:
        if k.find('md5')==-1:
            log.info( "checking file " + k )
            fileurlpath = inUrl + k
            downloadfilepath = inLocalPath + k
            downloadcount=0
            while downloadcount < 5:
                if os.path.isfile(downloadfilepath):
                    log.info( "file {} is existed".format(downloadfilepath) )
                    kmd5 = getFileMD5(downloadfilepath)
                    log.info( "file {} md5 is : expected {}, actual{}.".format(k, MD5[k],kmd5))
                    if kmd5 == MD5[k] :
                        log.info( " File {} is already downloaded, pass MD5 check. skip now".format(k) )
                        scpRemoveFile(k,inRemotePath)
                        break
                    else :
                        log.info ( "File {} isn't correct, plan to remove file".format(downloadfilepath))
                        os.remove(downloadfilepath)
                log.info("begin scp file {}, count {}".format(downloadfilepath, downloadcount))
                scpfile(k,inRemotePath,inLocalPath)
                downloadcount +=1
            if config['extract'] and verifyFileMD5(downloadfilepath):
                extractFile(inDownloadPath, k, inDownloadPath)


def downloadFiles(inUrl, inDownloadPath, inCycle=5) :
    Files=getFileList(inUrl,inCycle)
    MD5 = getFileMd5List(inUrl, Files, inCycle)

    for k in Files:
        #if k.startswith('pricing') :
        #    continue
        if k.find('md5')==-1:
            log.info("checking file {}".format(k))
            fileurlpath = inUrl + k
            downloadfilepath = inDownloadPath + k
            downloadcount=0
            while downloadcount < inCycle:
                if os.path.isfile(downloadfilepath):
                    log.info( "file {} is existed".format(downloadfilepath))
                    kmd5 = getFileMD5(downloadfilepath)
                    log.info( "file {} md5 is : expected {}, actual{}.".format(k, MD5[k],kmd5))
                    if kmd5 == MD5[k] :
                        log.info( "file {} md5 checking is passing, skip download".format(k))
                        break
                    else :
                        log.info ("plan to remove file" + downloadfilepath )
                        os.remove(downloadfilepath)
                log.info( "begin download file {}, count {}".format(downloadfilepath, downloadcount))
                downloadFile(inUrl+k,inDownloadPath)
                log.info( "finish download file {}, count {}".format(downloadfilepath, downloadcount))
                downloadcount += 1
            if config['extract'] and verifyFileMD5(downloadfilepath):
                extractFile(inDownloadPath, k, inDownloadPath)


if __name__ == '__main__':
    me = singleton.SingleInstance()
    args=sys.argv[1:]
    optlist, remainargs = getopt.getopt(args, 'fised', ['fulldate=','incrementaldate=','downloadpath='
                                ,'scppath=','scphost=','scpuser=','scpkey=','cycle=','debugfile=','fulloffset=','incoffset='])
    if len(optlist)==0:
        print '''
        python downloadEPFFiles.py -i -f -s -e --fulldate 'incrementaldate','downloadpath'
                                    ,'scppath','scphost','scpuser','scpkey','cycle','debugfile','offset'
        '''
        exit(1)

    config={}
    config['full']=False
    config['incremental']=False
    config['scp']=False
    config['extract']=False
    config['fulldate']=""
    config['incrementaldate']=""
    config['baseurl']='https://feeds.itunes.apple.com/feeds/epf/v3/full/'
    config['downloadpath']=os.getcwd()
    config['scppath']='/Users/administrator/epf/'
    config['scphost']='10.1.97.56'
    config['scpuser']='administrator'
    config['datafolder']='epfdata/'
    config['incrementalfolder']=config['datafolder']+'incremental/'
    config['cycle']=5
    config['user']= 'epfuser99877'
    config['password']= 'd2ab676b93f4d4ae74355dedffb108b4'
    config['debugfile']='downloadEPF.log'
    config['fulloffset']=0
    config['incoffset']=0
    config['usescpkey']=False
    config['scpkey']=""
    config['offsetflag']=0
    config['debug']=False

    for n, v in optlist:
        if n == '-f':
            config['full'] = True
        elif n == '-i':
            config['incremental']=True
        elif n == '-d':
            config['debug']=True
        elif n == '-e':
            config['extract']=True
        elif n == '-s':
            config['scp']=True
            import paramiko
        elif n == "--fulldate":
            config['fulldate'] = v
        elif n == "--incrementaldate":
            config['incrementaldate'] = v
        elif n == "--downloadpath":
            config['downloadpath'] = v
        elif n == "--debug":
            config['debugfile'] = v
        elif n == "--cycle":
            config["cycle"]=int(v)
        elif n == "--fulloffset":
            config["fulloffset"]=int(v)
        elif n == "--incoffset":
            config["incoffset"]=int(v)
        elif n == "--scphost":
            config["scphost"]=v
        elif n == "--scpuser":
            config["scpuser"]=v
        elif n == "--scppath":
            config["scppath"]=v
        elif n == "--scpkey":
            config['usescpkey']=True
            config["scpkey"]=v
        else :
            print "the unknown parameter {} is {}".format(n,v)
            continue

    debuglevel=logging.INFO
    if config['debug']:
        debuglevel=logging.DEBUG

    log = logging.getLogger('')
    log.setLevel(debuglevel)

    ch=logging.StreamHandler()
    ch.setLevel(debuglevel)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)

    log.info("optlist is {}" + str(optlist))
    fh=logging.FileHandler(config['debugfile'],encoding='utf8')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    log.addHandler(fh)
    if config['scp']:
        if config['full']:
            url=getFullUrlPath(inFullDate=config['fulldate'],inOffset=config['fulloffset'])
            remotefolder=config['scppath']+config['datafolder']
            localfolder=config['downloadpath']+'/'+config['datafolder']
            log.info("Start SCP Full File Mode, url is {}, remote folder is {}, local path is {}".format(url,remotefolder,localfolder))

        if config['incremental']:
            url=getIncrementalUrlPath(inFullDate=config['fulldate'],inFullOffset=config['fulloffset'],inIncrementalDate=config['incrementaldate'],inIncrementalOffset=config['incoffset'])
            remotefolder=config['scppath']+config['incrementalfolder']
            localfolder=config['downloadpath']+'/'+config['incrementalfolder']
            log.info("Start SCP Incremental File Mode, url is {}, remote folder is {}, local path is {}".format(url,remotefolder,localfolder))

        scpFiles(url, remotefolder, localfolder)

    else :
        if config['full'] and not config['scp']:
            url=getFullUrlPath(inFullDate=config['fulldate'],inOffset=config['fulloffset'])
            log.info("Start Full File Mode, path is {}".format(url))
            downloadFiles(url,config['downloadpath']+'/'+config['datafolder'])
        if config['incremental']:
            url=getIncrementalUrlPath(inFullDate=config['fulldate'],inFullOffset=config['fulloffset'],inIncrementalDate=config['incrementaldate'],inIncrementalOffset=config['incoffset'])
            log.info("Start Incremental File Mode, path is {}".format(url))
            downloadFiles(url,config['downloadpath']+'/'+config['incrementalfolder'])
